Det finnes en funksjon drawTerrain, som tegner terrenget. Hvis vi åpner konsoll i browseren (F12 -> Console) og redefinerer drawTerrain, ser vi flagget nede ved horisonten:

```
function drawTerrain() {}
```
