# not+cipher

Vi har funksjonene chr() og int() tilgjengelig, samt * for multiplikasjon og ** for potens og + for addisjon. Husk også å bytte ut + med %2B (url-enkoding) før du sender inn programmet.

```
encode(s):
 e = ""
 for ch in s:
  c = "chr("
  for i in range(ord(ch)): c+="+int()**int()"
  c += ")+"
  e += c
 return e[:-1] # Fjern siste pluss
```

Til sist sender vi inn programmet vårt: `print(*open(<generert_streng>))`
Husk gjøre URL-enkoding, altså å bytte ut + med %2b før du sender inn programmet.

