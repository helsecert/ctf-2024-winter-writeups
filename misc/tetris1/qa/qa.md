Oppgave:
1. I denne runden av HelseCTF har vi laget vårt eget tetris-spill. Spill tetris til du har fått rydda bort totalt fem linjer.
2. Før du starter spillet står det hvilke taster som kan brukes for å styre spillet. Én av tastene er ikke listet og gir uante muligheter. Bruk tasten mye og få et flagg.
3. Nede i horisonten, under fjell, snø og terreng ligger det et flagg.
4. Spill til du har fått renska 5000 linjer på brettet.


# QA av tetris

Jeg løse de slik, i denne rekkefølgen (litt vanskelig å vite hvilket flagg man fant)

# flag: drawSky - helsectf{himmelferd}
editerer `drawFrame(fb);` slik at vi kun tegner ett og ett lag. Med `drawSky(fb);` fant jeg et flagg.

# flag: drawTerrain -- helsectf{tetromino} (Detter er kanskje 5linjer-win flagget?)
Finner alle `drawBitMap` og følger x=drawBitMap og i=x og har allerede sett at state[99] og state[98] ser spennende ut. Kjører tegningen av disse + substring fra ene fugelen for å finne et flagg.
```
    i($, 140, 145, state[99]),
      drawTerrain.a || (drawTerrain.a = 2),
      i($, 197, 145, y(brd1[0].substring(269))),
      i($, 254, 145, state[98]);     
```

# flag: ArrowUp -  helsectf{meisebolle}
Følger hintet på punkt2 for å se på ArrowUp. Ser at jeg må ha passert linje 14 for å få satt beenBelow=true for at tasten skal ha en funksjon. Setter derfor `beenBelow=true;` i `keyPress` og får da fort ut flagget på arrow up.

Ser også at jeg kan printe den rett ut slik jeg tok "drawTerrain"-flagget ved å kjøre denne i `drawBird`: x($,130,120,y("\\D6;bbW3L((8((8,@7gGg^Aggg_eG7eggCWgggG]fL<9,LX99,9<A=?=^c==E===U-,?CU<<9==.]_?EV]?EE==c99,:a=-,9-97("));

# flag: helsectf{nocheat}
Jeg burde nok ha lest hintet på 5000 linjer, men jeg var ikke helt sikker på hvilke flagg jeg hadde funnet på alle stegene over. Siden cheat hadde så mye data og at jeg nettopp hadde løst bird med substring så tenkte jeg at det måtte være et ekstra bilde i cheat-strengen. Jeg brukte derfor en del tid på å forstå BitMap.decode og Bitstream, og regnet ut max offset fra `this.d.charCodeAt($)`. Sjekket på bird flagget at den maxet til var det 268. Planen var å gjøre det samme på cheat for å finne et offset i den for å printe enda et flagg. Skuffelsen var stor da hele cheat-strengen ble brukt på å printe ut cheat-bildet :(

Nå gikk jeg tilbake til 5000 linjers hintet og løste den på noen min etter at jeg _faktisk_ leste litt mer kode :D ser at currentMode settes til noe ugyldig slik at c(); kalles i den main loopen i bunn. Patcher derfor vekk disse samtidig som jeg setter at jeg har tatt 5000 linjer i removeLines():
```
    lines= 5000;
    state[0] = 5000;
```

Den her ville jo ha løst 2 flagg i en smell om jeg hadde starta her.

Fant til slutt nocheat flagget her:
```
 q = W[parseInt(gameovers[c + 492])];
      for (let d = 0; d < 7; d++)
        q % 2 && setPixel($, c + 130, d + 100, 0, 0, 0),
          (q = Math.floor(q / 2));
```

