# not+cipher

Vi har funksjonene chr() og int() tilgjengelig, samt * for multiplikasjon og ** for potens og + for addisjon. Husk også å bytte ut + med %2B (url-enkoding) før du sender inn programmet.

Intended solve var å utnytte at `int()**int() == 1`. Med dette kan vi lage uttrykk
som `chr(int()**int()+int()**int()+...)` for å lage de tegnene vi ønsker, og med å bruke multiplikasjon og potenser inni der kan man gjøre uttrykkene mye kortere.

Det viste seg å være en litt tungvint måte å gjøre det på, siden også `not()==True` og `(not())+(not()) == True+True == 2`. For å lage tallet 1 kan man også bruke `+(not())` i stedet for `int(not())`. Mens `int()` er det korteste for 0.

I tillegg til dette kan man bruke f.eks. `int(repr(+(not()))+repr(int())+repr(int()))` for å lage tallet 100.

Til sist kan man gange alle dobbeltkonsonanter med 2 i stedet for å skrive dem to ganger:

```
chr(uttrykk_for_tall)*((not())+(not()))
```

Det korteste mulige (tror vi) programmet for å åpne og skrive innholdet i fila er 1967 bytes.

Nå viste det seg at det er en bug i oppgaven. Vi hadde nemlig lagt en kopi av flagget i current directory, så du trenger bare enkode "flagg.txt", noe som gjør koden atskillig kortere, og den kan derfor løses med 646 bytes.

Kjør `python3 dynamisk/dyn2.py` for å se korteste enkoding av hver enkelt byte.
