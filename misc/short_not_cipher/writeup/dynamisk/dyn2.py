import math
import sys
from io import StringIO


if len(sys.argv) > 1:
    count = max(int(sys.argv[1]), 127)
else:
    count = 127

# optimaliseringer
# run length encoding av dobbelt bokstaver (i det andre programmet)
# 0 = int()
# 1 = (not())
# tall kan kombineres med *, +, og ** (potens), f.eks 2**7 = 128
# de kan også lages med int(repr(1)*3) = 111
# vi bygger dynamisk opp for å regne ut korteste streng som gir et tall
# dette programmet prøver alle kombinasjoner for å konstruere neste tall

def runcode(code):
    old_stdout = sys.stdout
    redirected_output = sys.stdout = StringIO()
    exec(code)
    sys.stdout = old_stdout
    return(redirected_output.getvalue())

def run_length(string):
    output = []
    i = 0
 
    while (i <= len(string)-1):
        count = 1
        ch = string[i]
        j = i
        while (j < len(string)-1):
            if (string[j] == string[j+1]):
                count = count+1
                j = j+1
            else:
                break
        output.append((ch, count))
        i = j+1
    return output

computed = []
for i in range(0, count + 1):
    computed.append((10000, "", False, "", False))

zero = "int()"
one = "(not())"
COST = 0
STRING = 1
IS_ADDED = 2
COMMENT = 3
INTED = 4

computed[0] = (len(zero), zero, False, "0", False)
computed[1] = (len(one), one, False, "1", False)

for i in range(0, count + 1):
    if computed[i][COST] < 10000:
        continue

    if i > 9:
        # repr
        as_string = str(i)
        res = []
        comments = []
        runs = run_length(as_string)
        # print("runs %d, %s" % (i, str(runs)))
        had_runs = False
        for c, n in runs:
            digit = int(c)
            cs = computed[digit][STRING]
            if digit == 1:
                # cs = "int%s" % cs
                cs = "+%s" % cs
        
            re = "repr(%s)" % cs
            if n > 1:
                had_runs = True
                ncs = computed[n][STRING]
                if computed[n][IS_ADDED]:
                    ncs = "(%s)" % ncs
                re = "%s*%s" % (re, ncs)
                comments.append("'%d'*%d" % (digit, n))
            else:
                comments.append("'%d'" % digit)
            res.append(re)
            
        full_repr = "int(%s)" % ("+".join(res))
        comment = "int(%s)" % ("+".join(comments))
        cost = len(full_repr)
        if cost < computed[i][COST]:
            # print("%6d : %s" % (i, full_repr))
            computed[i] = (cost, full_repr, False, comment, True)

        if had_runs:
            # if it had runs, it might still be the case that having each character separately is faster anyways
            # (like 0)
            res = []
            comments = []
            for c in as_string:
                digit = int(c)
                cs = computed[digit][STRING]
                if digit == 1:
                    # cs = "int%s" % cs
                    cs = "+%s" % cs
            
                re = "repr(%s)" % cs
                comments.append("'%d'" % digit)
                res.append(re)
            
            full_repr = "int(%s)" % ("+".join(res))
            comment = "int(%s)" % ("+".join(comments))
            cost = len(full_repr)
            if cost < computed[i][COST]:
                # print("%6d : %s" % (i, full_repr))
                print("had runs but was simpler: %d, delta: %d" % (i, cost - computed[i][COST]))
                computed[i] = (cost, full_repr, False, comment, True)
    
    for j in range(1, i):
        # addition
        k = i - j
        cost = computed[j][COST] + computed[k][COST] + 1
        if cost < computed[i][COST]:
            # computed[i] = (cost, "%s+%s" % (computed[j][STRING], computed[k][STRING]), True, "%s+%s" % (computed[j][COMMENT], computed[k][COMMENT]))
            computed[i] = (cost, "%s+%s" % (computed[j][STRING], computed[k][STRING]), True, "%d+%d" % (j, k), False)
        
        #multiplication
        if j > 1 and i % j == 0:
            k = i // j
            cost = computed[j][COST] + computed[k][COST]
            a = computed[j][STRING]
            b = computed[k][STRING]
            if computed[j][IS_ADDED]:
                a = "(%s)" % a
                cost += 2
            if computed[k][IS_ADDED]:
                cost += 2
                b = "(%s)" % b

            if cost < computed[i][COST]:
                # computed[i] = (cost, "%s*%s" % (a, b), False, "(%s)*(%s)" % (computed[j][COMMENT], computed[k][COMMENT]))
                computed[i] = (cost, "%s*%s" % (a, b), False, "%d*%d" % (j, k), False)

        #power
        if j > 1 and math.log(i, j).is_integer():
            k = int(math.log(i, j))
            cost = computed[j][COST] + computed[k][COST] + 6
            if cost < computed[i][COST]:
                a = computed[j][STRING]
                b = computed[k][STRING]
                # computed[i] = (cost, "(%s)**(%s)" % (a, b), False, "((%s)**(%s))" % (computed[j][COMMENT], computed[k][COMMENT]))
                computed[i] = (cost, "(%s)**(%s)" % (a, b), False, "(%d**%d)" % (j, k), False)
    # c, s, _, cm = computed[i]
    # print("%6d : %7s : %3d : %-180s : %s" % (i, repr(chr(i)), c, s, cm))
                
    if i >= 100:
        # special 3 digit cases:
        as_string = str(i)
        res = []
        comments = []

        # split 1
        a1 = (as_string[:2])
        b1 = (as_string[2:])
        # split 2
        a2 = (as_string[:1])
        b2 = (as_string[1:])
        for aa, bb in [(a1, b1), (a2, b2)]:
            a = int(aa)
            b = int(bb)
            if str(a) != aa or str(b) != bb:
                continue
            a_str = computed[a][STRING]
            b_str = computed[b][STRING]
            if computed[a][INTED]:
                a_str = a_str[4:-1]
            elif a == 1:
                # a_str = "repr(int%s)" % a_str
                a_str = "repr(+%s)" % a_str
            else:
                a_str = "repr(%s)" % a_str

            if computed[b][INTED]:
                b_str = b_str[4:-1]
            elif b == 1:
                # b_str = "repr(int%s)" % b_str
                b_str = "repr(+%s)" % b_str
            else:
                b_str = "repr(%s)" % b_str
            
            rep = "int(%s+%s)" % (a_str, b_str)
            cost = len(rep)

            if i == 116 and False:
                print("116 = %d + %d cost: %d, delta: %d" % (a, b, cost, cost - computed[i][COST]))
                print("a: %s" % a_str)
                print("b: %s" % b_str)
                print(rep)
                print(computed[i][STRING])

            if cost < computed[i][COST]:
                print("improvement! : %d = %d + %d : %3d : %s => %s" % (i, a, b, cost - computed[i][COST], computed[i][STRING], rep))
                computed[i] = (cost, rep, False, "int(r(%d) + r(%d))" % (a, b), True)

for i in range(0, count + 1):
    c, s, f, cm, it = computed[i]
    code = "print(str(%s))" % s
    res = runcode(code)[:-1]
    works = res == str(i)
    # print("%3d : [%5s] (res:%4s) : %6s : %3d : %-100s : %s" % (i, str(works), res, repr(chr(i)), c, s, cm))
    print("%3d : %6s : %3d : %-100s : %s" % (i, repr(chr(i)), c, s, cm))

content = ", ".join(["'%s'" % s for _, s, _, _, _ in computed])
print('codes = [%s]' % content)

