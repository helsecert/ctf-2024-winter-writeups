# short not+cipher

Denne oppgaven er helt lik not+cipher, bare at programmet må være kortere enn TODO tegn.

Utvikler Carl I. Pher trenger av og til å kjøre python-programmer på webserveren sin. Derfor har han laget en veldig enkel webtjeneste for å sende inn python-programmer og returnere output:

Eksempelkjøring av program: http://server/?p=print(repr(repr))

For å hindre misbruk er kun 13 forskjellige tegn tillatt i python-programmet: `not+cipher(*)`

Dessverre viser det seg at dette ikke er godt nok. Hent ut flagget i `/lol/hemmeligmappe/flag.txt`
