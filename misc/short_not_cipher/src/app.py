# app.py
from flask import Flask, request
import subprocess

app = Flask(__name__)

@app.route("/")
def default():
    program = request.args.get('program')
    if not program:
        return 'Send inn et program, f.eks: /?program=print(print)\n'
    elif len(program) >= 5000:
        return 'Programmet ditt må være kortere enn 5000 tegn. Det du sendte inn var %s tegn.' % len(program)
    elif not all([x in 'not+cipher(*)' for x in program]):
        illegal = set()
        for ch in program:
            if ch not in 'not+cipher(*)':
                illegal.add(hex(ord(ch)))
        return 'Du har ulovlige tegn i programmet ditt: ' + " ".join(illegal) + '\n'
    else:
        try:
            return subprocess.check_output(['python', '-c', program])
        except:
            return "Programmet ditt feila på et eller annet vis. Enten feil syntaks eller runtime error.\n"
