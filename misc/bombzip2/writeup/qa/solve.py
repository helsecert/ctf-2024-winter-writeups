"""
"The compressed blocks in bzip2 can be independently decompressed, without having to process earlier blocks. This means that bzip2 files can be decompressed in parallel"

så vi bare skipper alle blocker unntatt den siste, lager ny .bz2 fil, fixer litt CRC og pakker ut.

$ python solve.py

$ bzip2recover newfile.bz2
bzip2recover 1.0.8: extracts blocks from damaged .bz2 files.
bzip2recover: searching for block boundaries ...
   block 1 runs from 80 to 723
bzip2recover: splitting into blocks
   writing block 1 to `rec00001newfile.bz2' ...
bzip2recover: finished

$ bunzip2 -d rec00001newfile.bz2 
$ strings rec00001newfile
helsectf{b0mb3rm4n_s3nt_y0u_a_fl4g}

"""
f = open("flag.bz2","rb").read()
magic = f[0:2] # 'BZ' signature/magic number
version = f[2:3]  # 'h' for Bzip2 ('H'uffman coding), '0' for Bzip1 (deprecated)
hundred_k_blocksize = f[3:4] # '1'..'9' block-size 100 kB-900 kB (uncompressed)
compressed_magic = f[4:10]
last_block_idx = f.rfind(compressed_magic)
newfile = magic + version + hundred_k_blocksize + f[last_block_idx:]
open("newfile.bz2","wb").write(newfile)
