#Mulig løsning

```
tryeng@box:~$ time bzcat flag.bz2 | tail -c 1000
helsectf{b0mb3rm4n_s3nt_y0u_a_fl4g}

real    747m44,213s
user    711m37,121s
sys     148m22,753s
tryeng@box:~$
```

# Bedre løsning
Hvis man ser i fila finner man mange like blokker med data. Den siste blokka er annerledes, og her ligger flagget. Alle blokker i bzip2 er uavhengige av hverandre, så ved å bare dekomprimere den siste blokka, kan flagget hentes ut.

I stedet for å bruke 12 timer kan man da hente ut siste 100 kB av fila og bruke `bz2recover` for å få ut data.

Man kan også programmatisk hente ut kun den siste blokka, bruke bz2recover på den og pakke ut flagget. Se solve.py og solve.sh.
