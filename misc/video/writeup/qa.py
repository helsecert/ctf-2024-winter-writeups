chal_hash = "$2b$14$WpeU3BB4A1FpeOoTRa100O3/a5RSzhnvFJyxOEZ3v2z4It/gM71Eu"
null,prefix,rounds,b64 = chal_hash.split("$")
salt = b64[0:22]
h = b64[22:]
mysalt = f"$2b$14${salt}".encode()
print(salt)

import bcrypt
def enc(password):
    return bcrypt.hashpw(password, mysalt)    

# grep -a " " rockyou.txt > rockyou_spaces.txt
fd = open("rockyou_spaces.txt","rb")
for line in fd.readlines():
    password = line[:-1]
    parts = password.split(b" ")
    lens = " ".join([str(len(x)) for x in parts])
    cnt = password.count(b" ")        
    if password.count(b" ") == 5 and len(parts[0]) == 5:
        print(f"cnt:{cnt} lens:{lens}", [password])
        if lens == "5 2 6 5 2 5":
            hash = enc(password)        
            print(hash)
            if hash == chal_hash.encode():        
                print("\n\nsolved")
                flag = f"helsectf{{{password.decode()}}}"
                print("flag=   ", flag)
                quit()
            

