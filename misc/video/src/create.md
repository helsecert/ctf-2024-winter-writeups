

`echo "passord" | mkpasswd -s --method=bcrypt -R 10`
lag bcrypt passord. Det skal ikke være mulig å løse oppgaven med bruteforce, så bruk 10+ bcrypt runder. Hvis du løser oppgaven rett, så skal du bare trenger å prøve en hash.


`ffmpeg -i video.mp4 -i music.mp3 -i typing.mp3 -map 0:v -map 1:a -map 2:a -c copy -shortest output.mp4`
Lag mp4 fil med 2 spor der spor 1 er musikken, og spor 2 er lyden av trykking.
Den korteste filen bestemmer lengden på output filen, så trykking lyden må være den korteste.

Legg til div hint i exif data til mp4 filen. Dette kan gjøres med ffmpeg, eller med å gjøre det manuellt med vlc 

