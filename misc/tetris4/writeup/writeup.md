Her gjelder det å få antall linjer til å komme over 5000. Dette ligger lagret både i variabelen `lines` og i `state[0]`. I tillegg sjekker vi at antall brikker lagt, `nTetros`, er 2 og en halv ganger størrelsen til antall linjer.

Åpne konsoll, skriv følgende:

```
nTetros=15000;
lines=6000;state[0]=6000;
```

Voila!
