# null pointer

Utvikler Per Ointer trenger av og til å kjøre python-programmer på webserveren sin. Derfor har han laget en veldig enkel webtjeneste for å sende inn python-programmer og returnere output:

Eksempelkjøring av program: http://server/?p=print(repr(repr))

For å hindre misbruk er kun 10 forskjellige tegn tillatt i python-programmet: `pointer(*)`

Dessverre viser det seg at dette ikke er godt nok. Hent ut flagget i fila `0` i current directory.
