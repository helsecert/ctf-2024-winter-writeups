Kari bruker 3 timer på å kjøre 150km, så hun kjører i `50km/t`. Ola bruker 5 timer, så han kjører i `30km/t`.

Siden Kari starter 1 time før Ola har hun allerede reist 50km når Ola starter. De har nå 100km mellom seg og sammenlagt kjører de i `(50km/t + 30km/t =) 80km/t`. De bruker derfor 1,25t på distansen:
```
  100km / 80km/h = (100km/80km)h = 1,25h = 1time og 1/4 = 1time 15min
```
De møtes derfor `kl 1415`, og Kari har da kjørt i 2t15min med en fart på 50km/t. Hun har tilbakelagt `112.5km = 112500`. Vi kan sjekke dette ved at Ola har kjørt i 1t15min med en fart på 30km/t = 37.5km, og 150km - 37.5km = 112.5km.

Svaret er derfor: `helsectf{1415_112500}`