"""
Dette er et klassisk RSA signature forgery angrep og har flere løsninger.

Vi kan f.eks. be som å få signert m*2. Siden vi kjenner N,e så kan vi lage m' = m * 2^e (mod N) og får den signert som s' = (m*2^e)^d = m^d * 2^(e*d) = m^d * 2 (mod N).
Vi kan nå (lokalt hos oss) finne s =  s'*2^-1 (mod N). Årsaken til dette er at 2^(e*d) = 2^1. ed=1 mod phi(n) er basisen i hvordan RSA fungerer.

Et annet angrep er å be om å få signert to ulike meldinger m0,m1 for å få signaturene s0,s1, også kombinerer vi s0,s1 slik at vi får en gyldig s.

Angrepet er mulig fordi vi kan signere hva vi vil (bortsett fra meldingen m) og meldingen er ikke enkodet. Et mottiltak til denne type angrep er å enkode meldingen som skal signeres (f.eks. med et hash).
"""
from Crypto.Util.number import inverse, bytes_to_long, long_to_bytes
from pwn import *

contract = b"Dette er en superviktig kontrakt for veeldig viktige ting med store ord og uforstaaelige kruseduller."

io = remote("helsectf2024-2da207d37b091b1b4dff-kontraktsignering.chals.io", 443, ssl=True, level="DEBUG")  

z = io.recvuntil(b"N=")
N = int(io.recvline().strip().decode())
print("N=", N)
io.sendline(b"sign")
io.sendline(long_to_bytes((bytes_to_long(contract)*pow(2,0x10001,N))).hex().encode()) # s' = m * 2^e (mod N)
z = io.recvuntil(b"sign=")
print(z)
s_ = bytes_to_long(bytes.fromhex(io.recvline().decode().strip()))
print(s_)

io.sendline(b"verify")
io.sendline(long_to_bytes((s_ * inverse(2,N))%N).hex().encode()) # s = s' * 2^-1 (mod N)

#io.interactive()
data = io.recvall().decode()
print(data)
