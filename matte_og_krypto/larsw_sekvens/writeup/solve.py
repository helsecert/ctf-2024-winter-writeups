alfa = "AAABAACAADAAEAAFAAGAAHAAIAAJAAKAALAAMAANAAOAAPAAQAARAASAATAAUAAVAAWAAXAAYAAZAAaAAbAAcAAdAAeAAfAAgAAhAAiAAjAAkAAlAAmAAnAAoAApAAqAA"
ct = """AjAA
  AiAA
  kAAl
  AAnA
  AiAA
  hAAi
  AnAA
  iAAj
  pAAq
  QAAR
  AAlA
  AYAA
  LAAM
  AgAA
  AgAA
  AAiA
  AiAA
  WAAX
  mAAn
  nAAo
  jAAk
  AZAA
  AlAA
  LAAM
  AqAA"""

flag = [chr(alfa.find(c.strip())) for c in ct.split("\n")]
print("".join(flag))
