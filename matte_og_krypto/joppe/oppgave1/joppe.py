import random
import signal

denials = [
    "Makan, dette skal ikke være så vanskelig!",
    "Huff, var det for vanskelig?",
    "Nah, den må du lengre ut på landet med!",
    "Nixi pixi, det var ikke riktig",
    "Det var ikke riktig, prøv igjen!",
    "Ha ha ha, seriøst? Nei",
]

accepts = [
    "Ja ja, det var kanskje for lett? Min hemmelighet er: {}",
    "Ja, det var riktig! Her er min hemmelighet: {}",
    "Det var riktig! Ta hemmeligheten min: {}",
    "Du er god, du vil trenge denne: {}",
]

shares = [(-500, 1229), (500, 2229), (1000, 2729)]
riddles = [
    {
        "question": "De fattige har det, de rike trenger det, og hvis du spiser det, dør du. Hva er det?",
        "answer": ["ingenting", "nothing", "null", "zero", "nada"],
    },
    {
        "question": "En bussjåfør kom nedover en gate. Han suste rett forbi et stoppskilt, uten å stoppe. Han tok til venstre der det var innkjøring forbudt. Han passerte en politibil på feil side av gata. Likevel brøt han ikke en eneste trafikkregel. Hvordan kunne det ha seg?",
        "answer": ["fotgjenger", "gikk", "fots", "gående"],
    },
    {
        "question": "Hva blir våtere jo mer den tørker?",
        "answer": ["håndkle", "dopapir", "handduk", "håndduk", "tørk", "tørkepapir", "serviett"]
    },
]

flag = "helsectf{ved_x_null_er_alt_gull}"


def contains_any(string, substrings):
    for substring in substrings:
        if substring in string:
            return True
    return False


def handle_timeout(sig, frame):
    raise TimeoutError('')


def play():
    signal.alarm(60)
    signal.signal(signal.SIGALRM, handle_timeout)
    solved = {}

    while True:
        riddleNo = input(
            "\nVelg gåte (1-3) eller 4 for å teste koden: ").lower()
        try:
            riddleNo = int(riddleNo)
        except ValueError:
            continue

        if (riddleNo < 1 or riddleNo > 4):
            continue

        if riddleNo == 4:
            code = input("Gi meg koden så skal jeg gi deg flagget: ").lower()
            if code in ["1729"]:
                print(flag)
                return
            else:
                print("Beklager, det var ikke riktig kode")
            continue

        riddle = riddles[riddleNo-1]
        print(riddle["question"])
        answer = input("Ditt svar: ").lower()
        print()
        if contains_any(answer, riddle["answer"]):
            print(random.choice(accepts).format(shares[riddleNo-1]))
            solved[riddleNo] = True
        else:
            print(random.choice(denials))

        if len(solved) >= 2:
            print("\nDu har nå løst nok gåter til å finne koden :)")


if __name__ == "__main__":
    try:
        play()
    except TimeoutError:
        print("\n\nDu bruker for lang tid! prøv igjen senere.")
