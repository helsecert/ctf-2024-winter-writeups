
from Crypto.Util.number import inverse, bytes_to_long, long_to_bytes
from pwn import *

oppgavenavn = "joppe1"
io = remote(
    f"helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io", 443, ssl=True, level="ERROR")
io.sendafter("for å teste koden: ".encode(), b"1\n")
io.sendafter("Ditt svar: ".encode(), b"ingenting\n")
io.recvuntil(b": (")
result = io.recvline().strip()[:-1]
print(result)
assert result == b"-500, 1229"

io.sendafter("for å teste koden: ".encode(), b"2\n")
io.sendafter("Ditt svar: ".encode(), b"gikk\n")
io.recvuntil(b": (")
result = io.recvline().strip()[:-1]
print(result)
assert result == b"500, 2229"

shares1 = [(-500, 1229),(500, 2229)]
import shamir
from Crypto.Util.number import long_to_bytes
import shamir

secret = shamir.recover_secret(shares1, prime= 2**521 - 1)
assert secret == 1729

io.sendline(b"4\n1729")
io.recvuntil(b"gi deg flagget:")
data = io.recvline()
print(data)
