✖ nc localhost 5000

Velg gåte (1-3) eller 4 for å teste koden: 1
De fattige har det, de rike trenger det, og hvis du spiser det, dør du. Hva er det?
Ditt svar: ingenting

Ja ja, det var kanskje for lett? Min hemmelighet er: (2, 7)

Velg gåte (1-3) eller 4 for å teste koden: 2
En bussjåfør kom nedover en gate. Han suste rett forbi et stoppskilt, uten å stoppe. Han tok til venstre der det var innkjøring forbudt. Han passerte en politibil på feil side av gata. Likevel brøt han ikke en eneste trafikkregel. Hvordan kunne det ha seg?
Ditt svar: gikk

Ja ja, det var kanskje for lett? Min hemmelighet er: (3, 9)
Du har løst nok gåter til å finne flagget!

Velg gåte (1-3) eller 4 for å teste koden: 4
Gi meg koden så skal jeg gi deg flagget: 0
Beklager, det var ikke riktig kode

Velg gåte (1-3) eller 4 for å teste koden: 4
Gi meg koden så skal jeg gi deg flagget: 1
Beklager, det var ikke riktig kode

Velg gåte (1-3) eller 4 for å teste koden: 4
Gi meg koden så skal jeg gi deg flagget: 2
Beklager, det var ikke riktig kode

Velg gåte (1-3) eller 4 for å teste koden: 4
Gi meg koden så skal jeg gi deg flagget: 3
helsectf{ved_x_null_er_alt_gull}


