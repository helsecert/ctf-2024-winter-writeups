
from Crypto.Util.number import inverse, bytes_to_long, long_to_bytes
from pwn import *

oppgavenavn="joppe3"
io = remote(f"helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io", 443, ssl=True, level="ERROR")
answers = {"1":"barnebarn","2":"12","3":"fyrstikk","4":"stearinlys","5":"hansker"}

def solve_quiz(answers):
    shares = []
    for k,v in answers.items():
        io.sendafter("Velg gåte (1-5):".encode(), k.encode()+b"\n")
        io.sendafter("Ditt svar: ".encode(), v.encode()+b"\n")
        io.recvuntil(b": (")
        result = io.recvline().strip()[:-1].decode()
        #print(result)
        a,b = result.split(", ")
        shares.append( (int(a),int(b)) )
    return shares

#shares = solve_quiz(answers)

shares= [ (2344332245, 65530548079400370318528292311996782073761367365025193477153),
          (13429121073, 362422730191176971056722614457277134831341186224587467857920), 
          (3429121073, 95529569869860840087763619573109073266430740016361100443349),
          (23429121073, 611905828855656382575774271667026570700762200408325616089780),
          (267429121073, 1304263530048391400503348909036069075396369750743127072608747),
        ]
print("shares=", shares)
R.<x> = PolynomialRing(QQ)
poly3 = R.lagrange_polynomial(shares)
print(long_to_bytes(int(poly3(0))))
flag = long_to_bytes(abs(int(poly3(323454343234))))
print(flag)

"""
b'x=323454343235'
b'helsectf{Jimmy_Ekelun\xa2\xec'

"""