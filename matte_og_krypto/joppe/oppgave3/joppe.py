import random
import signal

denials = [
    "Makan, dette skal ikke være så vanskelig!",
    "Huff, var det for vanskelig?",
    "Nah, den må du lengre ut på landet med!",
    "Nixi pixi, det var ikke riktig",
    "Det var ikke riktig, prøv igjen!",
    "Ha ha ha, seriøst? Nei",
]

accepts = [
    "Ja ja, det var kanskje for lett? Min hemmelighet er: {}",
    "Ja, det var riktig! Her er min hemmelighet: {}",
    "Det var riktig! Ta hemmeligheten min: {}",
    "Du er god, du vil trenge denne: {}",
]

shares = [
    (2344332245, 65530548079400370318528292311996782073761367365025193477153),
    (13429121073, 362422730191176971056722614457277134831341186224587467857920),
    (3429121073, 95529569869860840087763619573109073266430740016361100443349),
    (23429121073, 611905828855656382575774271667026570700762200408325616089780),
    (267429121073, 1304263530048391400503348909036069075396369750743127072608747)]

riddles = [
    {
        "question": "Hva er det dine barn må gi deg som du ikke kan få selv?",
        "answer": ["barnebarn", "barne"],
    },
    {
        "question": "Noen måneder har 30 dager og noen har 31 dager. Hvor mange måneder har 28 dager?",
        "answer": ["12", "tolv", "alle"],
    },
    {
        "question": "Du kommer til en kald vinterhytte og oppdager at du kun har en fyrstikk. I hytta er det en peis, en parafinlampe og ett stearinlys. Hva tenner du først?",
        "answer": ["fyrstikk", "fystikken"],
    },
    {
        "question": "Livet mitt kan måles i timer, jeg tjener ved å bli slukt. Tynn, jeg er rask. Feit, jeg er treg. Vind er min fiende. Hva er jeg?",
        "answer": ["stearinlys", "sterinlys"],
    },
    {
        "question": "De har ikke kjøtt, fjær eller bein. Likevel har de fingre og tommel. Hva er de?",
        "answer": ["hansker", "handske", "vott", "votter"],
    }
]


def contains_any(string, substrings):
    for substring in substrings:
        if substring in string:
            return True
    return False


def handle_timeout(sig, frame):
    raise TimeoutError('')


def play():
    signal.alarm(60)
    signal.signal(signal.SIGALRM, handle_timeout)
    solved = {}

    while True:
        riddleNo = input("\nVelg gåte (1-5):").lower()
        try:
            riddleNo = int(riddleNo)
        except ValueError:
            continue

        if (riddleNo < 1 or riddleNo > 5):
            continue

        riddle = riddles[riddleNo-1]
        print(riddle["question"])
        answer = input("Ditt svar: ").lower()
        print()
        if contains_any(answer, riddle["answer"]):
            print(random.choice(accepts).format(shares[riddleNo-1]))
            solved[riddleNo] = True
        else:
            print(random.choice(denials))

        if len(solved) >= 3:
            print("\nDu har løst nok gåter til å finne flagget!")


if __name__ == "__main__":
    try:
        play()
    except TimeoutError:
        print("\n\nDu bruker for lang tid! prøv igjen senere.")
