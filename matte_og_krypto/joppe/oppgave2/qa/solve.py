
from Crypto.Util.number import inverse, bytes_to_long, long_to_bytes
from pwn import *

oppgavenavn="joppe2"
io = remote(f"helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io", 443, ssl=True, level="ERROR")
answers = {"1":"'","2":"7","4":"meg","5":"navnet"}

def solve_quiz(answers):
    shares = []
    for k,v in answers.items():
        io.sendafter("Velg gåte (1-5):".encode(), k.encode()+b"\n")
        io.sendafter("Ditt svar: ".encode(), v.encode()+b"\n")
        io.recvuntil(b": (")
        result = io.recvline().strip()[:-1].decode()
        #print(result)
        a,b = result.split(", ")
        shares.append( (int(a),int(b)) )
    return shares
shares = solve_quiz(answers)
#shares= [(4745696227450807, 655305480794027129181307180701455045712682321660286466368078), (2588682506107567, 655305480793967733879479427128553132958736140573542016023878), (4359708773407619, 655305480794013934214261797861718778141983069985929089695418), (6634057562378419, 655305480794107806938926839990618969191800755240410150653418)]
print("shares=", shares)
from Crypto.Util.number import long_to_bytes
import shamir

secret = shamir.recover_secret(shares, prime= 2**521 - 1)
print(secret, long_to_bytes(secret).decode())
