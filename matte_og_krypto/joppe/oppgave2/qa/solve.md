# 1
Velg gåte (1-5):1
Æ e ", å [ er lik, ka e æ?
Ditt svar: '

Ja, det var riktig! Her er min hemmelighet: (4745696227450807, 655305480794027129181307180701455045712682321660286466368078)

Velg gåte (1-5):

# 2

Velg gåte (1-5):2
Hvis 'away away away' er klokka 3. Hva er 'come bye'?
Ditt svar:


manuell bruteforce 0..1..2...

Velg gåte (1-5):2
Hvis 'away away away' er klokka 3. Hva er 'come bye'?
Ditt svar: 7

Ja ja, det var kanskje for lett? Min hemmelighet er: (2588682506107567, 655305480793967733879479427128553132958736140573542016023878)

# 3
Velg gåte (1-5):3
Don't worry, be happy. Etter I kommer ii, men hva følger?
Ditt svar:

# 4
Velg gåte (1-5):4
Hvem er det som er sønn av mine foreldre, men likevel ikke min bror?
Ditt svar: meg

Det var riktig! Ta hemmeligheten min: (4359708773407619, 655305480794013934214261797861718778141983069985929089695418)
Du har løst nok gåter til å finne flagget!


# 5
Velg gåte (1-5):5
Hva er ditt, men blir likevel brukt mest av andre?
Ditt svar: navnet

Du er god, du vil trenge denne: (6634057562378419, 655305480794107806938926839990618969191800755240410150653418)
Du har løst nok gåter til å finne flagget!

# solve
from Crypto.Util.number import long_to_bytes
import shamir
shares = [
    (4745696227450807, 655305480794027129181307180701455045712682321660286466368078),
    (2588682506107567, 655305480793967733879479427128553132958736140573542016023878),    
    # skjønte aldri nr3
    (4359708773407619, 655305480794013934214261797861718778141983069985929089695418),
    (6634057562378419, 655305480794107806938926839990618969191800755240410150653418),
]
secret = shamir.recover_secret(shares)
print(secret, long_to_bytes(secret))
# 655305480793942574876234195691695898011105414377060925858173 b'helsectf{muldvarp_er_bra}'
