import random
import signal

denials = [
    "Makan, dette skal ikke være så vanskelig!",
    "Huff, var det for vanskelig?",
    "Nah, den må du lengre ut på landet med!",
    "Nixi pixi, det var ikke riktig",
    "Det var ikke riktig, prøv igjen!",
    "Ha ha ha, seriøst? Nei",
]

accepts = [
    "Ja ja, det var kanskje for lett? Min hemmelighet er: {}",
    "Ja, det var riktig! Her er min hemmelighet: {}",
    "Det var riktig! Ta hemmeligheten min: {}",
    "Du er god, du vil trenge denne: {}",
]


shares = [
    (4745696227450807, 655305480794027129181307180701455045712682321660286466368078),
    (2588682506107567, 655305480793967733879479427128553132958736140573542016023878),
    (4035090358829972, 655305480794003703185282913192696782073761367365025193477153),
    (4359708773407619, 655305480794013934214261797861718778141983069985929089695418),
    (6634057562378419, 655305480794107806938926839990618969191800755240410150653418),
]

riddles = [
    {
        "question": "Æ e \", å [ er lik, ka e æ?",
        "answer": ["tastatur", "layout", "keyboard", "'"],
    },
    {
        "question": "Hvis 'away away away' er klokka 3. Hva er 'come bye'?",
        "answer": ["7", "sju", "syv"],
    },
    {
        "question": "Don't worry, be happy. Etter I kommer ii, men hva følger?",
        "answer": ["iv", "4", "fire", "four"],
    },
    {
        "question": "Hvem er det som er sønn av mine foreldre, men likevel ikke min bror?",
        "answer": ["meg", "jeg", "i"]
    },
    {
        "question": "Hva er ditt, men blir likevel brukt mest av andre?",
        "answer": ["navn", "navnet"],
    }
]


def handle_timeout(sig, frame):
    raise TimeoutError('')


def contains_any(string, substrings):
    for substring in substrings:
        if substring in string:
            return True
    return False


def play():
    signal.alarm(60)
    signal.signal(signal.SIGALRM, handle_timeout)
    solved = {}

    while True:
        riddleNo = input("\nVelg gåte (1-5):").lower()
        try:
            riddleNo = int(riddleNo)
        except ValueError:
            continue

        if (riddleNo < 1 or riddleNo > 5):
            continue

        riddle = riddles[riddleNo-1]
        print(riddle["question"])
        answer = input("Ditt svar: ").lower()
        print()
        if contains_any(answer, riddle["answer"]):
            print(random.choice(accepts).format(shares[riddleNo-1]))
            solved[riddleNo] = True
        else:
            print(random.choice(denials))

        if len(solved) >= 3:
            print("\nDu har løst nok gåter til å finne flagget!")


if __name__ == "__main__":
    try:
        play()
    except TimeoutError:
        print("\n\nDu bruker for lang tid! prøv igjen senere.")
