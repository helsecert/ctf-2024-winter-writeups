# Oppgave 1

Du får ut en koordinat for hver gåte du løser.
Kooridnatene du kunne få var (-500, 1229), (500, 2229), (1000, 2729)

Har du to av disse kan du lage en rett linje.
Oppgave teksten sier at koden til safen er når du står på null - som
hintet til at du må finne verdien der x er null.

Stigningstallet til linjen er 1. Tar vi utgangspunkt i punktet (-500, 1229)
kan vi legge til 500 i x og y og stå igjen med koordinaten (0, 1729).

Koden 1729 vil gi oss flagget.

## Gåtene i denne oppgaven:

### Gåte 1

- De fattige har det, de rike trenger det, og hvis du spiser det, dør du. Hva er det?",
- Ingenting

### Gåte 2

- En bussjåfør kom nedover en gate. Han suste rett forbi et stoppskilt, uten å stoppe. Han tok til venstre der det var innkjøring forbudt. Han passerte en politibil på feil side av gata. Likevel brøt han ikke en eneste trafikkregel. Hvordan kunne det ha seg?",
- Han gikk

### Gåte 3

- Hva blir våtere jo mer den tørker?
- Et håndkle

# Oppgave 2

Her er konseptet likt som i oppgave 1 men nå må man ha tre punkter for å kunne finne koden.
Disse tre punktene definerer da en annengradsligning der koden er y verdien når x er null.
Navnet til vår antagonist i oppgavene hintet til at man kunne bruke Shamir's Secret Sharing Scheme for å finne koden.

Plugger man inn 3 av punktene inn i SSS solveren gitt i sss.py så vil flagget komme ut som en hex enkodet string.

## Gåtene i denne oppgaven:

### Gåte 1

- Æ e \", å [ er lik, ka e æ?
- Tastatur, layout, keyboard og ' ble godtatt

- Hvis 'away away away' er klokka 3. Hva er 'come bye'?
- 7, sju og syv (dette er gjetehund kommandoer)

- Don't worry, be happy. Etter I kommer ii, men hva følger?
- Iv, 4, fire of four ble godtatt (dette er musikk teori)

- Hvem er det som er sønn av mine foreldre, men likevel ikke min bror?
- Meg, jeg, i og eg ble godtatt

- Hva er ditt, men blir likevel brukt mest av andre?
- Navn og navnet ble godtatt

# Oppgave 3

Denne oppgaven er identisk med oppgave 2 med ett tillegg. Svaret man får ut en vanlig solve med SSS er en hex enkodet string som angir en x kooridnat.
Koden er da y verdien for denne x koordinaten. Det underliggede konseptet i SSS er lagrange interpolasjon som kan brukes til å finne y verdien for en vilkårlig x koordinat gitt n punkter (som her er 3).

Koden er nesten den samme som å løse ut for x = 0, men ikke alle SSS implementasjoner støtter dette.

Se reconstruct_secret2 i sss.py for å se hvordan dette kan gjøres.

Et par komplikasjoner i denne oppgaven var at presesjonen i divisjonene i lagrange interpolasjonen var veldig viktig. I python var trikset å sette getcontext().prec til rundt 150.
En annen komplikasjon var at hvis man brukte alle 5 shares i utregningen var det vanskligere å beholde nødvendig nøyaktighet. Det var bedre å bruke kun 3 punkter.

## Gåtene i denne oppgaven:

### Gåte 1

- Hva er det dine barn må gi deg som du ikke kan få selv?
- Barnebarn og barne ble godtatt

- Noen måneder har 30 dager og noen har 31 dager. Hvor mange måneder har 28 dager?
- 12, tolv og alle ble godtatt

- Du kommer til en kald vinterhytte og oppdager at du kun har en fyrstikk. I hytta er det en peis, en parafinlampe og ett stearinlys. Hva tenner du først?
- Fyrstikk og fystikken ble godtatt

- Livet mitt kan måles i timer, jeg tjener ved å bli slukt. Tynn, jeg er rask. Feit, jeg er treg. Vind er min fiende. Hva er jeg?
- Stearinlys og sterinlys ble godtatt

- De har ikke kjøtt, fjær eller bein. Likevel har de fingre og tommel. Hva er de?
- Hansker, handske, vott og votter ble godtatt
