import random
from decimal import Decimal, getcontext

FIELD_SIZE = 10**16


def reconstruct_secret2(shares, x):
    getcontext().prec = 150  # Set precision as needed

    sums = Decimal(0)

    for j, share_j in enumerate(shares):
        xj, yj = share_j
        l_denominator = 1
        l_numerator = 1
        prod = Decimal(1)
        for i, share_i in enumerate(shares):
            xi, _ = share_i
            if i != j:
                l_numerator *= x - xi
                l_denominator *= xi - xj
        prod *= yj * Decimal(l_numerator) / Decimal(l_denominator)
        sums += prod

    return int(sums)


def reconstruct_secret(shares):
    getcontext().prec = 150

    sums = Decimal(0)
    for j, share_j in enumerate(shares):
        xj, yj = share_j
        prod = Decimal(1)
        for i, share_i in enumerate(shares):
            xi, _ = share_i
            if i != j:
                prod *= Decimal(xi)/Decimal(xi-xj)

        prod *= yj
        sums += prod

    return int(sums)


def polynom(x, coefficients):
    """
    This generates a single point on the graph of given polynomial
    in `x`. The polynomial is given by the list of `coefficients`.
    """
    point = 0
    # Loop through reversed list, so that indices from enumerate match the
    # actual coefficient indices
    for coefficient_index, coefficient_value in enumerate(coefficients[::-1]):
        point += x ** coefficient_index * coefficient_value
    return point


def coeff(t, secret):
    """
    Randomly generate a list of coefficients for a polynomial with
    degree of `t` - 1, whose constant is `secret`.

    For example with a 3rd degree coefficient like this:
        3x^3 + 4x^2 + 18x + 554

        554 is the secret, and the polynomial degree + 1 is
        how many points are needed to recover this secret.
        (in this case it's 4 points).
    """
    coeff = [random.randrange(0, FIELD_SIZE) for _ in range(t - 1)]
    coeff.append(secret)
    return coeff


def generate_shares(n, m, secret):
    """
    Split given `secret` into `n` shares with minimum threshold
    of `m` shares to recover this `secret`, using SSS algorithm.
    """
    coefficients = coeff(m, secret)
    shares = []

    for i in range(1, n+1):
        x = random.randrange(1, FIELD_SIZE)
        shares.append((x, polynom(x, coefficients)))

    return shares


def generate_secret(text):
    hexStr = ''.join([hex(ord(c))[2:] for c in text])
    return int(hexStr, 16)


def int_to_text(num):
    hexStr = hex(num)[2:]
    return ''.join([chr(int(hexStr[i:i+2], 16)) for i in range(0, len(hexStr), 2)])


def oppgave3():
    # Final share is the point where the flag is located
    final_solve = generate_secret("helsectf{Jimmy_Ekelund}")
    final_solve_x = 323454343234
    final_share = (final_solve_x, final_solve)

    # first solve is the point you find with SSS
    first_solve = generate_secret(f'x={final_solve_x}')
    first_share = (0, first_solve)

    # third solve is the point that defines the second degree polynomial
    third_share = (
        2344332245, 65530548079400370318528292311996782073761367365025193477153)

    # The second degree polynomial is defined by these shares
    defining_shares = [first_share, final_share, third_share]

    # Now generate other shares to use in the challenge
    extra1_x = 13429121073
    extra1_y = reconstruct_secret2(defining_shares, extra1_x)
    extra3_x = 23429121073
    extra3_y = reconstruct_secret2(defining_shares, extra3_x)
    extra2_x = 3429121073
    extra2_y = reconstruct_secret2(defining_shares, extra2_x)
    extra5_x = 267429121073
    extra5_y = reconstruct_secret2(defining_shares, extra5_x)

    final_shares = [third_share, (extra1_x, extra1_y),
                    (extra2_x, extra2_y), (extra3_x, extra3_y), (extra5_x, extra5_y)]

    print(f'Shares: {", ".join(str(share) for share in final_shares)}')

    oppgave3_pool = random.sample(final_shares, 3)
    oppgave3_first_answer = reconstruct_secret(oppgave3_pool)
    print(f'First solve: {int_to_text(oppgave3_first_answer)}')

    oppgave3_second_answer = reconstruct_secret2(oppgave3_pool, final_solve_x)
    print(f'Second solve: {int_to_text(oppgave3_second_answer)}')


def oppgave2():
    m = 3

    oppgave2_shares = [(4745696227450807,
                        655305480794027129181307180701455045712682321660286466368078),
                       (2588682506107567,
                        655305480793967733879479427128553132958736140573542016023878),
                       (4035090358829972,
                        655305480794003703185282913192696782073761367365025193477153),
                       (4359708773407619,
                        655305480794013934214261797861718778141983069985929089695418),
                       (6634057562378419,
                        655305480794107806938926839990618969191800755240410150653418)
                       ]
    oppgave2_pool = random.sample(oppgave2_shares, m)
    print(
        f'Combining shares: {", ".join(str(share) for share in oppgave2_pool)}')
    oppgave2_answer = reconstruct_secret(oppgave2_pool)
    print(f'Reconstructed secret: {int_to_text(oppgave2_answer)}')


def oppgave1():
    m = 2
    oppgave1_shares = [(-500, 1229), (500, 2229), (1000, 2729)]
    oppgave1_pool = random.sample(oppgave1_shares, m)
    print(
        f'Combining shares: {", ".join(str(share) for share in oppgave1_pool)}')
    oppgave1_answer = reconstruct_secret(oppgave1_pool)
    print(f'Reconstructed secret: {oppgave1_answer}')


if __name__ == '__main__':
    oppgave1()
    oppgave2()
    oppgave3()
