import networkx as nx
import matplotlib.pyplot as plt
import math

# uppercases = "".join([chr(x) for x in range(ord('A'), ord('Z') + 1)]) + "ÆØÅ"
lowercases = "".join([chr(x) for x in range(ord('a'), ord('z') + 1)]) + "æøå"

plaintext = "\n".join(open("../src/klartekst.txt"))
ciphertext = "\n".join(open("../src/chiffertekst.txt"))

alphabet = lowercases

edges = []

for i in range(0, len(plaintext)):
    cc = plaintext[i]
    dd = ciphertext[i]
    
    if cc in alphabet:
        edges.append((cc, dd))

edge_stats = {}

for e in edges:
    if e in edge_stats:
        edge_stats[e] += 1
    else:
        edge_stats[e] = 1

for e, c in sorted(edge_stats.items(), key=lambda x: x[0][0]):
    a, b = e
    print("%s -> %s: %d" % (a, b, c))

edges_unique = list(edge_stats.keys())
edge_values = [edge_stats[x] for x in edges_unique]
G = nx.DiGraph()
G.add_nodes_from(alphabet)
G.add_edges_from(edges_unique)

centers = [(-1,1), (1,1), (0,-1)]
alphabets = ['adfghjklsæø', 'eiopqrtuwyå', 'bcmnvxz']
radius = 0.76

pos = {}

for c, a in zip(centers, alphabets):
    cx, cy = c
    n = len(a)
    deg = 2 * math.pi / n
    for i in range(n):
        ch = a[i]
        x = cx + radius * math.cos(i * deg)
        y = cy + radius * math.sin(i * deg)
        pos[ch] = (x, y)

# bruker automagisk spring layout
pos = nx.spring_layout(G)

edge_labels = nx.get_edge_attributes(G, 0)
nx.draw_networkx_edges(G, pos=pos, connectionstyle='arc3, rad = 0.1', arrows=True)
# nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edge_stats)
nx.draw_networkx_nodes(G, pos=pos, node_size=400, node_color='#00b4d9')
nx.draw_networkx_labels(G, pos=pos, font_size=14)
plt.draw()
plt.show()
