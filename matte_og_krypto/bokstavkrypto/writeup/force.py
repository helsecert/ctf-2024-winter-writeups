import itertools

lowercases = "".join([chr(x) for x in range(ord('a'), ord('z') + 1)]) + "æøå"

plaintext = [x for x in "".join(open("../src/klartekst.txt")) if x in lowercases]
ciphertext = [x for x in "".join(open("../src/chiffertekst.txt")) if x in lowercases]

groups = ['bcmnvxz', 'eiopqrtuwyå', 'adfghjklsæø']
min_key_length = [3, 5, 4]
possible_key_duplicate_characters = 3

def get_key(a, b, alpha):
    z = alpha.index(b) - alpha.index(a)
    return alpha[(z)%(len(alpha))]

for group, key_length in zip(groups, min_key_length):
    plain = [x for x in plaintext if x in group]
    chipr = [x for x in ciphertext if x in group]
    key_range_end = key_length + possible_key_duplicate_characters + 1
    b = False
    # print("prøver g: ", group, " k: ", key_length, key_range_end)
    for perm in itertools.permutations(group):
        if b:
            break
        # regn ut offsets
        offsets = "".join([get_key(a, b, perm) for a, b in zip(plain, chipr)])
        l = len(offsets)
        # print(offsets)
        # mål repetetiveness gitt key length
        for k in range(key_length, key_range_end):
            prefix = offsets[:k]
            m = l / k
            count = offsets.count(prefix)
            f = count / m
            # print(perm, offsets, prefix, f)
            if f > 0.95:
                print(perm, offsets, prefix, f)
                b = True
                break
