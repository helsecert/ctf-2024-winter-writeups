med viginere er det naturlig å se på offsets mellom bokstaver,
men hvis man gjør det her så finner man ikke noe klart repeterende mønster slik som viginere cipher har.

det er et hint i teksten til et kart med piler og kanter
vi prøver heller å lage en directed graph ved å tegne inn hvilke kanter man har i grafen som representerer hvilke bokstaver som blir mappet til en annen.
hvis en a i klarteksten har blitt kryptert til en g, er dette en kant i grafen.
vi holder gjerne også telling på hvor mange ganger det skjer, denne informasjonen kan være nyttig

spørsmål til grafen:
hvis dette var en vanlig viginere-cipher hvordan ville grafen ha sett ut?
hva forteller grafen vi har oss?
hvor mange disjunkte grafer er det? hvor mange disjunkte grafer ville det vært ved vanlig viginere?
hva sier antallet utgående eller inngående kanter fra en node oss om antallet distinkte symboler i nøkkelen?
pro gamer move: hvis det er vanskelig å se tydelig, se på symboler som ikke dukker opp i klarteksten (w, z), de vil kun ha inngående kanter
hvordan vil fordelingen på vekten til de utgående kantene være normalt sett hvis nøkkelen består av kun ulike symboler?
ser det ut som om det er samme nøkkelen som er brukt over alt?

vi ser på de disjunkte grafene hver for seg og ser at
i bokstavene "eiopqrtuwyå" har nøkkelen 5 distinkte symboler
i bokstavene "adfghjklsæø" har nøkkelen 4 distinkte symboler
i bokstavene "bcmnvxz" har nøkkelen 3 distinkte symboler

trikset nå er å skjønne at de tre disjunkte bokstav-mengdene er tre forskjellige grupper,
og at viginere - kryptering skjer parallelt og uavhengig i de tre gruppene.
dette er det hintet til i diktet

hvis man er heldig ser man umiddelbart at de tre bokstav-gruppene er de tre radene på et vanlig tastatur.
da kommer sortering av bokstavene naturlig, og man man kan se på offsets og får med en gang nøklene
("poteter", "klægg", "nvc")

hvis ikke, er man nødt til å finne en sortering og nøkler på en annen måte.
dette kan man løse med brute force. (se vedlagt program)

