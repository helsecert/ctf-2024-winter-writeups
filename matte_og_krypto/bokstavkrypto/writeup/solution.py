
# deler inn alfabetet i 3 deler
c1 = "qwertyuiopå"
c2 = "asdfghjkløæ"
c3 = "zxcvbnm"
charsets = [c1, c2, c3]

# decrypt character
def decrypt(state, keys, c):
    # finn hvilket charset den er i
    charset_index = -1
    index = -1
    for cs in range(len(charsets)):
        try:
            index = charsets[cs].index(c)
            charset_index = cs
            break
        except ValueError:
            continue
    
    if charset_index == -1:
        # c er ikke en bokstav
        return c
    
    key_index = state[charset_index]
    state[charset_index] += 1
    key_word = keys[charset_index]
    key_char = key_word[key_index % len(key_word)]
    charset = charsets[charset_index]
    offset = charset.index(key_char)
    new_index = (index - offset) % len(charset) # decrypt bruker - offset
    new_char = charset[new_index]
    return new_char

def dec_str(keys, mystring):
    output = ""
    key_counters = [0] * 3

    for c in mystring:
        e = decrypt(key_counters, keys, c)
        output += e
    return output

print("keys:")
keys = ["poteter", "klægg", "nvc"]
print(keys)
print()

input = """øqw xli tx fgzl uv æylxaaxgvx
lqx dqw xqfpxuyy
ul gghå ggzh xuxzti
kx æt yw såo ksti

øt øyw gåso spfu oi
cqz æåo ti st åaaq
øåi dxåi tx nåk lo
skq uv lxztv up ayl wtafu

hæl jta uu gæue
cåj epsuy qa mlvqq
så agx gkyu xiy
bqq ux so gzuy

csåw øåo aåi mæzhatgås
hucz læåizt q aypiuuy
swst xyxzqq uy ju
btx mguq lås qbtkswc

håueåipza gæv sdluø
fætuxq xåj fildu
bux ål kqw øåz skåqu
oiqv åo txuhuy eglw!

hwyux pwatb jyswy lqu lcye, byvaunuuupw gjteplbqvs tkåe.
xrssk åu zuepq tæ uyæo zåxhybeåooi ktpaylw vwx pz qqapb.
gqxågz toql blahg gpzywy. qzuustu ætzwwy oåueyi tho, lykmåææ ngwtåh glzoa eåhpqqu to.
jåxyb iyiuk, uhpi xywæu håsgqnpitætz bæsuhokææ, suå xåhg nthuåuæot pwvg, tx nåsqåukiq ærq bæoiph ylqw xueåt.
wwkåøuzu uu cåzqzsrb xåssk, øts uståkqzø åilu. uuåæb nuxbyjw cpz togb ki mlwtåa.
xåicp pseqqnth llepya eqqhuqepq såv xqukø igåxbph hyw otiyk dqz wåxuuåyæh.

wøkøqksrh qø mpcuxæox æuzt. nrigmpoou gzntxllv uwaouyy cgvybrh.
qxuuatu æyæx wwyoåu, jylvtløqc tå hohiu zåz, ustcuzupb mtguåulot zypru.
voksgc wøpq øæituz, qwyig yv xthq ku, eåuewquwy xqhkyh vrsgl.
etksuxuyhpru pelox vypru, lonoåu tw æsqeågx stvæuqqqu, otcwta åå xgoiph. yetkb lo ktnooh zypru.
qigtlyve oålc tiæu, hbysqqqleåt øys xqøtnåsk tsuu, wggwåoyg ko qpwrø. zåvn ntk qvuywjrc subuål, tsye dgrzqzoh zåghæ.
jåvyn kwtzaqhkg qåyph kz prlc æsqeåti kwtzaqhkg. qøkøuksoh wuqouquqw drøoå xuk hgqåqx bybcwøå øwsgstl.
zphkg zuxhåtoko lb tuua bgwoph oyålettou. lkprtgc tehox vuepq, æåictzoob ye åhupduxj htø, xqhkyh tv wjpw.
quutx wyzkit yvyc åhqu, gushtbiæ{ætjsgqotøuu-aæzzgyqjøåamcsgg} lqu lcuu sgwymrh vqlp nwzxlksph yaqw.
cwymq qdæuqwig bgaxl zåv kwyyc iyiug, hwghpbåersqv såzwyiyh ålo tsuaugh.
"""

print(dec_str(keys, input))
