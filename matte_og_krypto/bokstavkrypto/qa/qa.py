#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
oppgaven er 3 Vignere som er flettet inn i hverandre. De har hvert sitt alfabet, og hver sin key.
hintet som ble lagt ut var for å understreke hintene som ligger i klartekst.txt:
- viginere
- piler og baner (analyse for å finne gruppene)
- "jobb gjerne i grupper, gode venner er de men bare seg imellom " -> distinkte grupper
- løse med kraft -> bruteforce
--> eller "uten et eneste tast" -> tastatur-gruppene

"""

klartekst = open("../src/klartekst.txt","rb").read().decode()
chiffertekst = open("../src/chiffertekst.txt","rb").read().decode()

import string
alfa = string.ascii_lowercase + "æøå"
def get_key(a,b,alpha=alfa):
    z = alpha.index(b) - alpha.index(a)
    return (z)%(len(alpha))

# find all plaintext[i] -> ciphertext[i] pairs
punct = string.punctuation + "\n "
map = {}
c = []
for i in range(len(klartekst)):
    src = klartekst[i]
    dst = chiffertekst[i]
    if src in punct: continue
    if not src in map: map[src] = {}
    if not dst in map[src]: map[src][dst] = 0
    map[src][dst] += 1
    c.append(dst)
k = sorted(list(map.keys()))
c = sorted(list(set(c)))

# show all jumps
for m in map:
    for x in map[m]:
        print(f"{m} -> {x} = ", map[m][x])
    print()

# we group all pt[i]:ct[i] pairs into k-groups of letters
group = []
def walk(ch):
    global group
    if not ch in group: group.append(ch)
    if ch in map:
        for k in map[ch].keys():
            if k in group: continue
            group = list(set(group + [k]))
            walk(k)   
    return group  
groups = []
for ch in k:
    group = []
    group = "".join(sorted(walk(ch)))    
    if not group in groups:
        groups.append(group)

"""
we find 3 distinct groups containing a unique set of letters

we also notice they correspond to the lines of the QWERTY keyboard.
we rearrange the order of the groups manually. this is the alphabets used later:
"""
print("groups", groups)
groups = [
    "asdfghjkløæ",
    "zxcvbnm",
    "qwertyuiopå",
]
print("groups", groups)

# analyze each group individually to learn the key(s)
# we extract 3 sets of plaintext:ciphertext text blocks
# by checking which group the pt[i]/ct[i] belongs to.
for gi in range(3):
    g = groups[gi]
    _pt, _ct = "",""
    key = ""
    for i in range(len(klartekst)):
        pt = klartekst[i]
        ct = chiffertekst[i]
        if ct in g:
            _pt += pt
            _ct += ct
            k = get_key(pt, ct, g)
            key += g[k]
            #print(g[k], end="")#end=" ")
    print(key)

keys = [
    "klægg",
    "nvc",
    "poteter",
]
kidx = [0]*3

# decrypt entire ciphertext
pt = ""
for i,ch in enumerate(chiffertekst):
    if ch in punct:                             # skip puncts
        pt += ch
        continue

    for gi in range(3):                         # find group
        if ch in groups[gi]: break  
    alfa = groups[gi]                           # alfabet
    ki = kidx[gi]                               # key index
    k = alfa.find(keys[gi][ki])                 # key mod
    kidx[gi] = (kidx[gi]+1)%len(keys[gi])       # update key to next round
    pt += alfa[(alfa.index(ch)-k)%(len(alfa))]  # decrypt
print("\n")
print(pt)
print()
print(pt[pt.find("helsectf{"):pt.find("}")+1])
