Løsningen er å forstå teksten og sette opp problemet som 2 lineære likninger med 2 ukjente:
```
  kari   == ola + 3133713
  kari+5 == 4*(ola + 5)
```
Vi må huske på at begge to øker sin alder med 5 år.

Vi kan nå løse ut likningene ved å sette inn for kari med penn&papir:
```
         kari + 5 = 4*(ola + 5)
                               # setter inn for kari = ola+3133713
ola + 3133713 + 5 = 4*ola + 20
 3133713 + 5 - 20 = 4*ola - ola
          3133698 = 3*ola
                               # deler på 3 på begge sider
          1044566 = ola
                               # setter inn ola for å finne kari
             kari = 1044566 + 3133713
             kari = 4178279
```

Eller vi kan løse det i et matterammeverk som kan løse symbolske likninger, som Sagemath. Vi løser likningene for å finne `kari,ola`:
```python
sage: kari,ola = var("kari,ola")
sage: eqs = [
        kari   == ola+3133713,
        kari+5 == 4*(ola+5)
      ]
sage: solve(eqs, kari, ola)
[[kari == 4178279, ola == 1044566]]
```

Flag: helsectf{4178279_1044566}