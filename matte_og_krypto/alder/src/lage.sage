x = 3133712
while True:
    kari,ola = var("kari,ola")
    eqs = [
            kari   == ola+x,
            kari+5 == 4*(ola+5)
        ]
    s=solve(eqs, kari, ola)[0]
    k = s[0].rhs()
    o = s[1].rhs()
    if k.is_integer():
        print(k,o)
        break
    x += 1
print("x=", x)    
print("kari=", k)
print("ola=", o)
assert k == o+x
assert k+5 == 4*(o+5)