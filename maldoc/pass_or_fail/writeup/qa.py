# https://ss64.com/nt/syntax-substring.html
data = open("pass_or_fail_replaced.bat","rb").read()
lines = data.split(b"\r\n")
newscript = [ lines[0] ]

buffers= {
    "public" : b"C:\Users\Public",
    "FOO"  : b"cNA1GeICBSROnTuU4od6ptf5ZXiV@vw37K0MLkW8ysFH2rlmqJDah9QgjPb x=zYE",
    "BAR"  : b"ezDNoFswKQ15qPlOVjmEG2khT=6U dSYfuHcXaBip0RLJ7WZxMv@9Ab3Iry4Cg8nt",
    "DEAD" : b"aWU5EyHQ@3YkZmj=At6 RJS4gGhqwMzBilvxX9rDbpuKcVC01fIn2s7L8FedPToON",
    "BEEF" : b"Vyz4Bqht=We7r lD@sbKkonvjOGgLU0XIcwFiAEZMY58uN9PTp32QafCxmS6R1dHJ",
    "BABE" : b"3@eT6vhkS=IEwWZ18jzYAXCaRx5H2n stMqyBrcfg4FbNKuOLpGiU9m7dD0lJPoQV",
    "abc1" : b"https://youtu.be/qybUFnY7Y8w?feature=68656c73656374667b746869735f325f7368616c6c5f706173737d",
    "abc2" : b"https://youtu.be/YZf0Q-v3u-k?feature=6e6569746865722069732074686973",
    "abc3" : b"https://youtu.be/3xYXUeSmb-Y?feature=54686973206973206e6f742074686520666c6167",
}

def read_line(idx):
    i = 0 
    line = lines[idx]
    scriptlinje = b""
    while i < len(line):    
        #print(">>>", i)#, chr(line[i]), line[i:i+5])
        if line[i] == ord("%"):
            x = line.find(b":~", i+1)
            if x == -1:
                x = line.find(b"%", i+1)
                var = line[i+1:x].decode()
                pt = f"%{var}%".encode()
                scriptlinje += pt
                i += len(pt)
                continue

            assert x != -1
            var = line[i+1:x].decode()
            if not "%" in var:                    
                end = line.find(b"%", i+7)
                a,b = line[x+2:end].decode().split(",")
                a,b = int(a), int(b)
                #print(x, var, end, a, b)            
                data = buffers[var]
                ch = data[a] # alt er ,1
                #print(x, var, end, a, b, ch, [chr(ch)])
                scriptlinje += chr(ch).encode()
                i = end+1
                continue

        scriptlinje += chr(line[i]).encode()
        i += 1


    print(scriptlinje.decode())
    newscript.append(scriptlinje)
    #print(b"\n".join(newscript).decode())    

    if b"feature=" in scriptlinje:
        code = scriptlinje.split(b"feature=")[1].decode()[:-1]
        print(code)
        print("!", bytes.fromhex(code))

#read_line(11)
#quit()
for i in range(len(lines)):
    #print([lines[i]],"\n\n")
    read_line(i)


# flag: helsectf{this_2_shall_pass}