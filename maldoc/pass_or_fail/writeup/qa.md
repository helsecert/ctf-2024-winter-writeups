Vi ser konturene av "SEt "克贝色斯=..." something. Bruker litt tid på å finne ut av %PUBLIC er. Finner ut at den betyr "C:\Users\Public"

Replacer 克贝色斯 til "FOO". Ser at neste linje også har en set så vi replacer 维艾豆贝 til BAR. Og neste 爱豆德豆 replaces til DEAD. Og 埃维艾阿 til BEEF. 色尔埃埃 til BABE.

Skriver en parser som decoder bat filen til dette:
```
@echo off
set "FOO=cNA1GeICBSROnTuU4od6ptf5ZXiV@vw37K0MLkW8ysFH2rlmqJDah9QgjPb x=zYE"
@set "BAR=ezDNoFswKQ15qPlOVjmEG2khT=6U dSYfuHcXaBip0RLJ7WZxMv@9Ab3Iry4Cg8nt"
@set "DEAD=aWU5EyHQ@3YkZmj=At6 RJS4gGhqwMzBilvxX9rDbpuKcVC01fIn2s7L8FedPToON"
@set "BEEF=Vyz4Bqht=We7r lD@sbKkonvjOGgLU0XIcwFiAEZMY58uN9PTp32QafCxmS6R1dHJ"
@set "BABE=3@eT6vhkS=IEwWZ18jzYAXCaRx5H2n stMqyBrcfg4FbNKuOLpGiU9m7dD0lJPoQV"
if 1 EQU 1 @echo off
if 1 EQU 1 set "abc1=https://youtu.be/qybUFnY7Y8w?feature=68656c73656374667b746869735f325f7368616c6c5f706173737d"
68656c73656374667b746869735f325f7368616c6c5f706173737d
! b'helsectf{this_2_shall_pass}'
if 91 LsS 111 set "abc2=https://youtu.be/YZf0Q-v3u-k?feature=6e6569746865722069732074686973"
6e6569746865722069732074686973
! b'neither is this'
if 1 EQU 1 set "abc3=https://youtu.be/3xYXUeSmb-Y?feature=54686973206973206e6f742074686520666c6167"
54686973206973206e6f742074686520666c6167
! b'This is not the flag'
if 43 EQU 0 (@exit) else set "abc4=G@nd@lf"
if 1 EQU 1 if not "%~1" equ "" goto usage
if 43 EQU 0 (@exit) else goto end
:us^age  ill%ﺹﮱس◯^﷽ﮢ%ii%ﻁﮢﻁﮕك^ﮱ%illl^l%ﯤ^سﯤسﯤﺖ%i   i%此已^行無這法%%貼息護保的^貼%%的貼法^凡被的%llilill^lll
if 91 LsS 111 if %1==%abc4% (start %abc1%) else (start %abc2%)
for /l %%y in (1 1 1) do exit
:en^d  i%(⊙ω⊙)(⊙ω⊙)(◕‿◕)(⊙ω⊙)(◕^‿◕)ヾ(⌐■_■)ノ%%ヾ(⌐■_■)ノ┌( ಠ_ಠ)┘^┌( ಠ_ಠ)┘(⊙ω⊙)(◕‿◕)ヾ(⌐■_■)ノ%llii^l%(⊙ω⊙)ヾ(⌐■_■)ノ(◕‿◕)ヾ(⌐■_■)ノ(◕‿◕)┌^( ಠ_ಠ)┘%iiili   ii%┌( ಠ_ಠ)┘┌( ಠ_ಠ)┘┌( ಠ_ಠ^)┘ヾ(⌐■_■)ノ(⊙ω⊙)(◕‿◕)%l%ヾ(⌐■_■)ノ(⊙^ω⊙)┌( ಠ_ಠ)┘┌( ಠ_ಠ)┘┌( ಠ_ಠ)┘ヾ(⌐■_■)ノ%^ii%ヾ(⌐■_■)ノヾ(⌐■^_■)ノヾ(⌐■_■)ノ(◕‿◕)ヾ(⌐■_■)ノ(⊙ω⊙)%lillli
if 1 EQU 1 start %abc3%
```

Tygger ut hex'ene som går til youtu.be og finner flagget: helsectf{this_2_shall_pass}