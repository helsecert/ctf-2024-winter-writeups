## Pass_or_fail solve

.bat filer kan se skikkelig kjipe ut og likevel kjøre.

Malicious .bat filer begynner gjerne med `@echo off` slik at alle kommandoer ikke utputes til terminal.
Vårt skript gjør også det. Om vi fjerne første `echo off` finner vi fort ut at det gjemmer seg en til obfuskert noen linjer ned... No matter. Om vi manuelt setter `echo` foran linjene ser vi fortsatt hva de gjør.

Vi finner blant annet disse tildelingene:
Linje 7, 8, 9, 10, 11
```
if 1 EQU 1 @echo off
if 1 EQU 1 set "abc1=https://youtu.be/qybUFnY7Y8w?feature=68656c73656374667b746869735f325f7368616c6c5f706173737d"
if 91 LsS 111 set "abc2=https://youtu.be/YZf0Q-v3u-k?feature=6e6569746865722069732074686973"
if 1 EQU 1 set "abc3=https://youtu.be/3xYXUeSmb-Y?feature=54686973206973206e6f742074686520666c6167"
if 43 EQU 0 (@exit) else set "abc4=G@nd@lf"
```
Her ser vi hva kommandoene deobfuskeres til (og hva de ville "kjørt" om vi ikke hadde puttet echo forran.)

Verdien bak feature forsvinner om vi forsøker å følge linken.
Om vi forsøker å dekode den. (hex-decoding) får vi følgende verdier:

[abc1](https://gchq.github.io/CyberChef/#recipe=From_Hex('None')To_Hex('Space',0/disabled)Remove_whitespace(true,true,true,true,true,false/disabled)&input=Njg2NTZjNzM2NTYzNzQ2NjdiNzQ2ODY5NzM1ZjMyNWY3MzY4NjE2YzZjNWY3MDYxNzM3Mzdk) (flagget), [abc2](https://gchq.github.io/CyberChef/#recipe=From_Hex('None')To_Hex('Space',0/disabled)Remove_whitespace(true,true,true,true,true,false/disabled)&input=NmU2NTY5NzQ2ODY1NzIyMDY5NzMyMDc0Njg2OTcz) og [abc3](https://gchq.github.io/CyberChef/#recipe=From_Hex('None')To_Hex('Space',0/disabled)Remove_whitespace(true,true,true,true,true,false/disabled)&input=NTQ2ODY5NzMyMDY5NzMyMDZlNmY3NDIwNzQ2ODY1MjA2NjZjNjE2Nw)

Flagg: `helsectf{this_2_shall_pass}`

For de med interesse er dette koden før obfuskering:

```
@echo off
set "abc1=https://youtu.be/qybUFnY7Y8w?feature=68656c73656374667b746869735f325f7368616c6c5f706173737d"
set "abc2=https://youtu.be/YZf0Q-v3u-k?feature=6e6569746865722069732074686973"
set "abc3=https://youtu.be/3xYXUeSmb-Y?feature=54686973206973206e6f742074686520666c6167"
set "abc4=G@nd@lf"
if not "%~1" equ "" goto usage
goto end
:usage
if %1==%abc4% (start %abc1%) else (start %abc2%)
exit
:end
start %abc3%
```
