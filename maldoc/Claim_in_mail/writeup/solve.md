## Claim_in_mail solve

Dette er en klassisk HTML-smuggling. Inspirert av dette Qakbot-eksemplet fra den virkelig verden: [6ebd7e86fa5a82fb062800f9529fede402ab4b758453ee1456197754ca051145](https://bazaar.abuse.ch/browse.php?search=sha256%3A6ebd7e86fa5a82fb062800f9529fede402ab4b758453ee1456197754ca051145)

Vi kan manuelt hente ut base64-blobber fra html. Dekode disse til svg-fil. Manuelt hente ut base64 derfra og dekode til attachment.zip. Men det er mye raskere å bare la HTML-doket gjøre jobben for oss. (Siden vi er kjent med HTML-docs og vet at uten en sårbarhet kan de ikke gjøre mer enn å droppe dokumenter. Som er det vi ønsker i dette tilfellet.)
Vi åpner HTML-en. Ser at passordet til zip-filen er *abc242*, og bruker det til å pakke ut zipfil. Det gir oss en .vhd. VHD kan også pakkes ut med 7z og vi gjør det og få en filstruktur. Her har vi en .lnk med kommando embedded inni. Følger vi kommando kommer vi inn i respondents/ og ser at filen *ibidem.cmd* kjøres. Denne setter noen variabler og sender disse inn i *suspect.cmd*. *Suspect.cmd* bruker variabler fra *ibidem* og renamer *crossbar.tmp* til *crossbar.exe* og kjører den med kommando run.
Herfra er det flere måter å løse oppgave på. Den enkle er å bruke strings til å se at crossbar.exe laster StringLibrary.dll. Og deretter bruker "strings -e l" til å dumpe ut en base64-string. Denne kan deretter dekodes i [cyberchef](https://cyberchef.org/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true,false)XOR_Brute_Force(1,100,0,'Standard',false,true,false,'helsectf')&input=REFFSUZ3RUhFQUlmRFFJN0RSQTdGUVVQRnpzSURROEJPd1U3QmdzUU8xSUJCZ0JUQVZ4U0FnVlJCVnhXQWdaVVVsWmNWRlFDWFZGV1hRSUJBQUZRVkZZRkJsQUdVMUZjVUZGWEFRRlZVRkZTVlYxVFUxRlFCd1ZVVVZWVlVGRVo) med litt prøving. 

En annen er å bruker dnSpy til å åpne StringsLibrary.dll og se hva som skjer. Deretter kan man entne kjøre crossbar.exe med riktig input ("giefKey"), eller manuelt dekode flagget tilsvarende over.

En tredje er god gammel reversing.