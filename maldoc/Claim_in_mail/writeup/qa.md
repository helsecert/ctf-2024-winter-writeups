# steg1
Åpner filen i firefox. "attachment.zip" dropper automatisk.

Alternativt er å åpne .html filen og bygge opp "tgTEDEOi" manuelt.

Eller åpne i firefox og dra ut innholdet i "tgTEDEOi" manuelt:
```
data:image/svg+xml;base64,PCFET0NUW...
```
Og dekode i f.eks. cyberchef.

# steg2
Tar ut CjRCOR6a, base64 dekoder og lagrer til attachment.zip. Pakker ut med passordet som står i Claim_3456.html "abc242"

# steg3
Flytter Claim_3456.vhd inn i en windows. Mounter opp fila og henter ut alle filene.

- Claim.lnk kjører `C:\Windows\SysWOW64\cmd.exe /q /c respondents\ibidem.cmd`
- skjult mappe "respondents"
```
├──  respondents
│  ├──  advisement.txt
│  ├──  crossbar.tmp
│  ├──  ibidem.cmd
│  ├──  StringLibrary.dll
│  ├──  substrates.jpg
│  └──  suspect.cmd
```

- ibidem.cmd:
```
set besiegingWaterproofing=naststargysystemx
set yawsAbolitionists=%besiegingWaterproofing:~4,4%
set counselWaltzer=%besiegingWaterproofing:~10,6%
set flightorfight=%besiegingWaterproofing:~14,1%%besiegingWaterproofing:~16,1%%besiegingWaterproofing:~14,1%
start /min respondents\suspect.cmd %counselWaltzer% %yawsAbolitionists% %flightorfight%
```
- suspect.cmd
```
set accuserRemedied=%windir%
set starlightPlacebos=%accuserRemedied%\\%132\\%2r32.exe
set fortressVeered=%temp%
set fluffyBunny="run"
set vigilantMutants=replace
set turtles=respondents\\crossbar.%3
:: taurusGleamed
%vigilantMutants% %starlightPlacebos% %fortressVeered% /A
ren %turtles:~1,21%.tmp %turtles%
call %2t %turtles%,%fluffyBunny%

exit
```
- substrates.jpg er bare bøll
- crossbar.exe er en dropper for å kjøre koden i `StringLibrary.dll`
- StringLibrary.dll åpnes med `dnSpy`:
```
using System;
using System.Runtime.CompilerServices;
using System.Text;

// Token: 0x02000003 RID: 3
internal class b64decode
{
        // Token: 0x06000003 RID: 3 RVA: 0x000020DC File Offset: 0x000002DC
        [NullableContext(1)]
        public static string b64_decode(string A_0, int A_1)
        {
                byte[] array = Convert.FromBase64String(A_0);
                int num = (A_1 * 2686 + 1432990190) % 256;
                for (int i = 0; i < array.Length; i++)
                {
                        byte[] array2 = array;
                        int num2 = i;
                        byte[] array3 = array2;
                        int num3 = num2;
                        array3[num3] ^= (byte)num;
                }
                return Encoding.UTF8.GetString(array);
        }
}



using System;
using System.Runtime.CompilerServices;
using System.Threading;
using RGiesecke.DllExport;

// Token: 0x02000002 RID: 2
public class HelseCTF
{
        // Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        [NullableContext(1)]
        [DllExport]
        public static void Main(string[] input)
        {
                bool flag = input[0] == "run";
                if (flag)
                {
                        Console.Write("Here I would normally run malware. Since this is a CTF I shall abstain");
                        Thread.Sleep(50);
                        Environment.Exit(0);
                }
                bool flag2 = input[0] == "giefKey";
                if (flag2)
                {
                        Console.Write(b64decode.b64_decode("DAEIFwEHEAIfDQI7DRA7FQUPFzsIDQ8BOwU7BgsQO1IBBgBTAVxSAgVRBVxWAgZUUlZcVFQCXVFWXQIBAAFQVFYFBlAGU1FcUFFXAQFVUFFSVV1TU1FQBwVUUVVVUFEZ", 5));
                        Thread.Sleep(500);
                        Environment.Exit(0);
                }
                else
                {
                        Console.Write("That is not the input I am looking for");
                }
        }
}
```


# solve

Her ser vi at strengen blir base64 decodet og xor'et med en statisk XOR key 0x64 (`(5 * 2686 + 1432990190) % 256`). Vi kan derfor løse denne i Python nå som vi vet at det også er XOR inkludert.

```
>>> import base64
>>> from pwn import xor
>>> xor(base64.b64decode("DAEIFwEHEAIfDQI7DRA7FQUPFzsIDQ8BOwU7BgsQO1IBBgBTAVxSAgVRBVxWAgZUUlZcVFQCXVFWXQIBAAFQVFYFBlAGU1FcUFFXAQFVUFFSVV1TU1FQBwVUUVVVUFEZ"), 100)
b'helsectf{if_it_qaks_like_a_bot_6ebd7e86fa5a82fb062800f9529fede402ab4b758453ee1456197754ca051145}'
```


# ghidra b64_decode
```
                             **************************************************************
                             *                          FUNCTION                          *
                             **************************************************************
                             char * __fastcall b64_decode(undefined4 A_0, byte * A_1,
             char *            EAX:4          <RETURN>
             undefined4        ECX:4          A_0
             byte *            EDX:4          A_1
             undefined4        Stack[0x4]:4   param_3
                             .NET CLR Managed Code
                             b64_decode
        004020e8 00 02 28        db[90]
                 13 00 00 
                 0a 0a 03 
           004020e8 [0]             0h,  2h, 28h, 13h,
           004020ec [4]             0h,  0h,  Ah,  Ah,
           004020f0 [8]             3h,  Bh,  7h, 20h,
           004020f4 [12]           7Eh,  Ah,  0h,  0h,
           004020f8 [16]           5Ah, 20h, EEh, B1h,
           004020fc [20]           69h, 55h, 58h, 20h,
           00402100 [24]            0h,  1h,  0h,  0h,
           00402104 [28]           5Dh,  Bh, 16h,  Ch,
           00402108 [32]           2Bh, 1Ah,  0h,  6h,
           0040210c [36]            Dh,  8h, 13h,  4h,
           00402110 [40]            9h, 11h,  4h, 8Fh,
           00402114 [44]           15h,  0h,  0h,  1h,
           00402118 [48]           25h, 47h,  7h, D2h,
           0040211c [52]           61h, D2h, 52h,  0h,
           00402120 [56]            8h, 17h, 58h,  Ch,
           00402124 [60]            8h,  6h, 8Eh, 69h,
           00402128 [64]           FEh,  4h, 13h,  5h,
           0040212c [68]           11h,  5h, 2Dh, DAh,
           00402130 [72]           28h, 14h,  0h,  0h,
           00402134 [76]            Ah,  6h, 6Fh, 15h,
           00402138 [80]            0h,  0h,  Ah, 13h,
           0040213c [84]            6h, 2Bh,  0h, 11h,
           00402140 [88]            6h, 2Ah
```

