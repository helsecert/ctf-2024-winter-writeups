using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;

namespace EasyFlag
{
    class VBAGenerator
    {
        public static void Main(string[] args)
        {
            if(args.Length == 2) {            
	    	    var outFile = new FileInfo(Directory.GetCurrentDirectory() + "\\" + args[0]);
                var vbaFile = new FileInfo(Directory.GetCurrentDirectory() + "\\" + args[1]);
                FillVBA(outFile, vbaFile);
            }                      
        }
        private static void FillVBA(FileInfo outFile, FileInfo vbaFile)
        {
            ExcelPackage pck = new ExcelPackage();
            var ws1 = pck.Workbook.Worksheets.Add("Sheet1");        
            ws1.Cells["A4"].Value = "oletools er kjekt når man jobber med maldocs";
            ws1.Cells["A5"].Value = "maldocs innholder normalt ondsinnet kode";
            ws1.Cells["A6"].Value = "ikke kjør dette på jobb-PC o.l.";
            
            //Create a vba project and set password permissions
            pck.Workbook.CreateVBAProject();
	    	pck.Workbook.VbaProject.Protection.SetPassword("38035eeccf1d4ccfd178");
            pck.Workbook.CodeModule.Code = File.ReadAllText(vbaFile.FullName);
            
            pck.SaveAs(outFile); // Save as xlsm
        }
    }
}
