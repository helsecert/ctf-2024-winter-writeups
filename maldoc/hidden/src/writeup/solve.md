Ligger et skjult ark i filen.
Kan vises med f.eks. en macro. Jeg kjører ofte denne på maldocs for å evt se om det er skjulte ark.
```
Sub Unhide
   Dim Doc As Object
   Dim Sheet As Object
   Dim i As Long
   Dim c As Long
   
   Doc = ThisComponent
   c = Doc.Sheets.Count
   for i = 1 to c
      Sheet = Doc.Sheets(i-1)
      Sheet.isVisible = True
   next i
End sub
```

Finner "Sheet1337" med `aGVsc2VjdGZ7dXN5bmxpZ19hcmshfQ==`

$ echo -n "aGVsc2VjdGZ7dXN5bmxpZ19hcmshfQ==" | base64 -d
helsectf{usynlig_ark!}
