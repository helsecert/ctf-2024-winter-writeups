using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;

namespace Hidden
{
    class VBAGenerator
    {
        public static void Main(string[] args)
        {
            if(args.Length == 2) {            
	    	    var outFile = new FileInfo(Directory.GetCurrentDirectory() + "\\" + args[0]);
                var vbaFile = new FileInfo(Directory.GetCurrentDirectory() + "\\" + args[1]);
                FillVBA(outFile, vbaFile);
            }                      
        }
        private static void FillVBA(FileInfo outFile, FileInfo vbaFile)
        {
            ExcelPackage pck = new ExcelPackage();
            var ws1 = pck.Workbook.Worksheets.Add("Sheet1");
            ws1.Cells["D5"].Value = "oida, ser ut som jeg har rotet bort flagget. hvor ble det av?";

            var ws2 = pck.Workbook.Worksheets.Add("Sheet1337");
            ws2.Cells["C4"].Value = "aGVsc2VjdGZ7dXN5bmxpZ19hcmshfQ==";
            ws2.Hidden = OfficeOpenXml.eWorkSheetHidden.Hidden ;

            pck.SaveAs(outFile); // Save as xlsm
        }
    }
}
