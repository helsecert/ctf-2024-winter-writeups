import numpy
import matplotlib.pyplot as plt
from PIL import Image

img = Image.open('../src/blåbær.png')
# img = Image.open('../image_processing_1/ekorn.png')
img_data = numpy.asarray(img)
print(img_data.shape)
w, h = img_data.shape

img_data = img_data * (1.0/255)

ft = numpy.fft.fft2(img_data)
fshift = numpy.fft.fftshift(ft)
spectrum = numpy.log(numpy.abs(fshift)) # ignorer denne

# så langt kom vi i oppgave 1

# her definerer jeg to masker vi kan bruke
# gaussian : mer glatt, høyst i midten og slakker av mot kantene
# square: en r x r firkant i midten

def gauss_2d(l, sigma):
    a = numpy.linspace(-(l - 1) / 2., (l - 1) / 2., l)
    gauss = numpy.exp(-0.5 * numpy.square(a) / numpy.square(sigma))
    kernel = numpy.outer(gauss, gauss)
    return kernel

def square(l, r):
    c = l // 2
    res = numpy.zeros((l, l), dtype=numpy.float64)
    for i in range(l):
        for j in range(l):
            if abs(i - c) < r and abs(j - c) < r:
                res[i, j] = 1
    return res

# velg mellom gauss eller firkant som mask
# vi ønsker å fjerne de frekvensene i midten, så vi setter filteret til 1 - mask
hp_filter = (1 - gauss_2d(2080, 20))
# hp_filter = (1 - square(2080, 10))

# gang de to sammen (det som ganges med 0 blir 0)
# langs kantene vil vi gange med tilnærmet 1, og i midten ganges med 0
new_transform = numpy.multiply(fshift, hp_filter)

# shift tilbake og ta invers fourier
new_unshifted = numpy.fft.ifftshift(new_transform)
new_image = numpy.fft.ifft2(new_unshifted).real

plt.subplot(221)
plt.imshow(img_data, cmap='gray')
plt.subplot(222)
plt.imshow(spectrum, cmap='gray')
plt.subplot(223)
plt.imshow(numpy.abs(hp_filter), cmap='gray')
plt.subplot(224)
plt.imshow(new_image, cmap='gray')
plt.show()
