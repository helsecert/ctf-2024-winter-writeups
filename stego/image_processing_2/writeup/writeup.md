vi fortsetter fra oppgave 1
vi har et forskjøvet frekvens-spekter

fshift = numpy.fft.fftshift(ft)

viktig å ignorere / hoppe over det siste steget, som er å ta absoluttverdien og visualisere
absoluttverdien mister en del informasjon, så vi kan ikke jobbe videre på den dataen,
men må jobbe på fourier-transformasjonen i stedet.

jeg hinter til i oppgaven at vi skal bruke et high pass filter, som slipper gjennom de høyere frekvensene
og fjerner lave. de lave frekvensene er i midten av diagrammet, og de høye mer mot kantene

rett og slett skal vi gange hele arrayet med et tilsvarende stort array (maske)
og så ta invers.
egentlig kan det være så lett som å sette alle verdiene i en n x n firkant i midten til 0
dette er en square mask

    l = # bredde på bildet
    r = 10 # størrelsen på firkanten, 
    c = l // 2
    for i in range(l):
        for j in range(l):
            if abs(i - c) < r and abs(j - c) < r:
                fshift[i, j] = 0

i min solve har jeg implementert square mask og gaussian mask som man kan velge mellom

skift tilbake og ta invers fourier

new_unshifted = numpy.fft.ifftshift(new_transform)
new_image = numpy.fft.ifft2(new_unshifted).real

tada

plt.imshow(new_image, cmap='gray')

alternativt kan oppgaven løses med en hvilkensomhelst high pass edge detection funksjon i et vilkårlig bildeprogram,
som f.eks gimp sin gausian high pass edge detection. dette er fordi denne funksjonen gjør nøyaktig det samme som løsningen her.
