from PIL import Image, ImageFont, ImageDraw  
import random


from PIL import Image, ImageFont, ImageDraw  
unicode_text = "HELSECTF{EN_PRIKK_KAN_HA_MANGE_FARGER!}"
font = ImageFont.truetype("/home/user/.local/share/fonts/NerdFonts/JetBrainsMonoNLNerdFont-SemiBold.ttf", 100)
text_width, text_height = font.getsize(unicode_text)
image = Image.new("RGB", (text_width+100, text_height+100), "black")
draw = ImageDraw.Draw(image)
draw.text((50, 50), unicode_text, fill=(0x8b, 0, 0), font=font)
image.save("flag.png")

img = Image.open("flag.png")
pix = img.load()
width, height = img.size
print("width:", width)
print("height:",height)

done = [ 0 ]

flag_coords = []
for x in range(width):
    for y in range(height):
        if pix[x,y][0] == 0x8b:
            flag_coords.append( (x,y) )
random.shuffle(flag_coords)
def get_flag_coords():
    x,y = flag_coords.pop()
    return x,y

def gif_frame():
    x,y=0,0
    while x+y in done:
        # 2/3 sannsynlig at vi finner 
        if random.randint(0,100) > 30:
            x,y = get_flag_coords()
            continue

        # hvis ikke, random søppel
        x = random.randint(0, width-1)
        y = random.randint(0, height-1)

    # avoid flag colors
    fill = 0x8b # 139
    size = 3
    if pix[x,y][0] != fill:
        size = 15+random.randint(0, 35)
        while fill > 130 and fill < 148:
            fill = random.randint(1, 0xff)
        fill = (fill,random.randint(0,256),random.randint(0,256))
    else:
        fill = (fill,0,0)
    
    image = Image.new("RGB", (width,height), "black")
    draw = ImageDraw.Draw(image)    
    draw.ellipse((x, y, x+size, y+size), fill=fill)
    done.append( (x,y) )
    print(".",end="")
    return image

def image(x, y, offset):
    image = Image.new("RGB", (2048, 512), "blue")
    draw = ImageDraw.Draw(image)
    draw.ellipse((x, y, x+offset, y+offset), fill="red")
    return image

def make_gif():
    frames = []
    print("creating frames")  
    for _ in range(4000):        
        frames.append(gif_frame())
    print("\ndone")

    print("saving...")
    frame_one = frames[0]
    frame_one.save("prikker.gif", format="GIF", append_images=frames,
                   save_all=True, duration=5, loop=1)
    # save_all = If present and true, all frames of the image will be saved. If not, then only the first frame of a multiframe image will be saved.
    # append_images = A list of images to append as additional frames. Each of the images in the list can be single or multiframe images.
    # duration = May not be present. The time to display the current frame of the GIF, in milliseconds.
    # loop = May not be present. The number of times the GIF should loop. 0 means that it will loop forever.
    print("done")

if __name__ == "__main__":
    make_gif()