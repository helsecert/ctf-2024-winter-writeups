from Crypto.Util.number import *
from PIL import Image
img = Image.open("../src/zellweger.png")
im = img.load()
w,h = img.size
bits = ""
for y in range(h):
    for x in range(w):
        p = im[x,y]
        for rgb in range(3):
            bits += str(p[rgb]&1)

# les av datastrømmen
import decabit
PULSE_BIT = "1"
PAUSE_BIT = "0"
parts = []
i = bits.find(PULSE_BIT, 0) # find start impulse
myflag = ""
while i < len(bits):
    part = bits[i+1:i+11]
    assert len(part) == 10        
    try:
        ch = decabit.decode_bits(part)
        myflag += chr(ch)
    except:
        break # hint part
    print(format(i, "5d"), part, part.replace("1","+").replace("0","-"), format(ch, "3d"), chr(ch))
    i = bits.find(PULSE_BIT, i+11) # find next start impulse    
#print("\n\n", myflag)
