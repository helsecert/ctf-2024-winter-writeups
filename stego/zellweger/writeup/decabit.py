import random
# https://www.dcode.fr/decabit-code
table="""0	--+-+++-+-	1	+--+++--+-	2	+--++-+-+-	3	+--+-++-+-
4	----+++-++	5	++--+++---	6	++--++--+-	7	++--+-+-+-
8	++---++-+-	9	---++++-+-	10	+-+-+++---	11	+-+-+-+-+-
12	+-+--++-+-	13	+---++-++-	14	+---++--++	15	--+++-++--
16	---++-+++-	17	+---+-++-+	18	+--++--+-+	19	+--++-+--+
20	+-+++--+--	21	+--+++-+--	22	++--+-++--	23	-+-++-++--
24	+--++--++-	25	+-+++-+---	26	++-+--++--	27	+-+-+-++--
28	+--+-+++--	29	+--+--++-+	30	+-++-++---	31	+-++-+-+--
32	+-+-++-+--	33	+---++++--	34	+-+--+-++-	35	+++--++---
36	+++--+-+--	37	+++---++--	38	++---+++--	39	--+-++++--
40	++--++-+--	41	-+-+-+-++-	42	++----+++-	43	+----+-+++
44	++---+-+-+	45	++-+-+-+--	46	++-+-+--+-	47	+++----++-
48	++--+--++-	49	+--+-+-++-	50	++++----+-	51	++-++---+-
52	+-+++---+-	53	-++++---+-	54	+-+-+---++	55	+++-++----
56	+++-+-+---	57	+-+-+--++-	58	-++-+--++-	59	+++-+----+
60	++++-+----	61	-+++-++---	62	-+-+-++-+-	63	++---++--+
64	++-+--+--+	65	++-+++----	66	++++--+---	67	+--++++---
68	-+-++++---	69	++-+--+-+-	70	-++---+++-	71	+---+-+++-
72	--+-+-+++-	73	+----++++-	74	--+--++++-	75	+++---+-+-
76	+-++---++-	77	+--+--+++-	78	--++--+++-	79	+-+---+-++
80	-+++--+-+-	81	-+-++-+-+-	82	-+++---++-	83	-+-++--++-
84	-+---++++-	85	-++++--+--	86	-++-++-+--	87	--++++-+--
88	--++-+++--	89	--++-+-++-	90	+-++++----	91	--++++--+-
92	--++-++-+-	93	+--+-+--++	94	+-++----++	95	-+-+++--+-
96	-++-+-+-+-	97	-+--++-++-	98	---+++-++-	99	-+--+-+++-
100	+---+++-+-	101	-+--+++-+-	102	+-+-++--+-	103	+--++-++--
104	++-++--+--	105	+-++--++--	106	+-+--+++--	107	-++--+++--
108	++---+-++-	109	++-+---++-	110	+++-+---+-	111	+++-+--+--
112	++-+-++---	113	++-++-+---	114	+-+---+++-	115	+-++--+-+-
116	-+-+--+++-	117	-+++-+-+--	118	+-++-+--+-	119	-++-+++---
120	+++--+--+-	121	+++++-----	122	-+++++----	123	--+++++---
124	---+++++--	125	----+++++-	126	++++++++++"""
table = table.replace("\n"," ").replace("	"," ").split(" ")

encode_map, decode_map = {},{}
for i in range(0, len(table),2):
    ascii = int(table[i])
    decabit = table[i+1]
    encode_map[ascii] = decabit
    decode_map[decabit] = ascii

PULSE_BIT = "1"
PAUSE_BIT = "0"
START_PULSE_BIT = "1"
START_PULSE_LENGTH = 1

PAUSE_BIT_LENGTH = (1+10)*3 # for å tydelig gi et signal på hva som er endret.

def encode(s):
    return encode_map[s]

def encode_string(data,sep=" "):
    if not isinstance(data,bytes):
        raise Exception("expected bytes")        
    decabit = []
    for part in list(data):
        if part > 126:
            raise Exception(f"only allowed 0 .. 126 byte, got {format(part,'02x')}")
        decabit.append(encode(part))
    if sep == None:
        return decabit
    return sep.join(decabit)

def encode_string_as_bits(s):
    """
    we add start pulse (SI) followed by 10 control pulses

    """
    parts = encode_string(s).replace("+",PULSE_BIT).replace("-",PAUSE_BIT).split(" ")
    bits = PAUSE_BIT*PAUSE_BIT_LENGTH
    for p in parts:
        bits += START_PULSE_BIT*START_PULSE_LENGTH
        bits += p
        
        # i telecom vet man jo ikke hvor lenge det er pause, mens man fører timer med gaffel! :-)
        bits += PAUSE_BIT*random.randint(PAUSE_BIT_LENGTH,1000)
    return bits

def decode(s):
    if not s in decode_map:
        raise Exception("unkown decabit code:", s)
    return decode_map[s]

def decode_string(data,sep=" "):
    ret = []
    for part in data.split(" "):        
        ret.append(decode(part))
    return bytes(ret)

def decode_bits(b):
    b = b.replace(PULSE_BIT,"+").replace(PAUSE_BIT,"-")
    return decode(b)

if __name__ == "__main__":
    hello_world_plaintext = b"HELLO WORLD"
    hello_world_decabit = "--+-+-+++- ++-+--+-+- +-++---++- +-++---++- +-+---+-++ +-+-++-+-- --++++-+-- +-+---+-++ -+++---++- +-++---++- -+-++++---"

    ct = encode_string(hello_world_plaintext)    
    pt = decode_string(hello_world_decabit)
    
    assert ct == hello_world_decabit    
    assert pt == hello_world_plaintext

    z = encode_string_as_bits(hello_world_plaintext)
    print(z)