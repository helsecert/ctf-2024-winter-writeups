Flagget er encodet med decabit:

 The decabit is a series of electrical pulses intended to encode data via power lines (alternative current). It is a modulation of the electrical ripple similar to the CPL (current carriers in line), mainly used in Germany and in Europe, for example for the transmission of meter readings.

Dette er enkodet inn i LSB i bildet, sekvensielt på alle 3 fargene med _masse_ pause
bits (0'ere) mellom hvert "tegn".

Hvis man bruker online tools som f.eks. https://stylesuxx.github.io/steganography/ så får man ut at det ligger data her. Alle andre LSB bits er nullet ut (hele bildet).
- det er sporadisk blokker med data og masse nullere mellom.
- i bunn ligger det et hint, en base64: aHR0cHM6Ly9kZS53aWtpcGVkaWEub3JnL3dpa2kvRGVjYWJpdA==
--> https://de.wikipedia.org/wiki/Decabit

Vi leser inn fila med PIL og dumper LSB bittene.
```python
img = Image.open("zellweger.png")
im = img.load()
w,h = img.size
bits = ""
for y in range(h):
    for x in range(w):
        p = im[x,y]
        for rgb in range(3):
            bits += str(p[rgb]&1)
print(bits[0:800])
```

Vi får får ut max 11 bit, og det starter alltid på 1. Som oppgitt i oppgaven, og med decabit så må vi
ha en Start Impulse for å signalere at det kommer data (1'er) så kommer det 10 bit ("decabit"):

33   11101100100000000000000000000000000000000000000000    <- S1101100100 h
547  10100111010000000000000000000000000000000000000000    <- S0100111010 e
794  11100010110000000000000000000000000000000000000000    <- S1100010110 l
1379 11011001010000000000000000000000000000000000000000    <- S1011001010 s
1459 10100111010000000000000000000000000000000000000000    <- S0100111010 e
1886 10100101110000000000000000000000000000000000000000    <- S0100101110 c
...

Hvis man tar ut alle bits og limer inn i https://www.dcode.fr/decabit-code så får man ut flagget (husk å fjerne Signal bittet som er først):
```
1101100100
0100111010
1100010110
1011001010
0100111010
0100101110 
..
```
= helsec

Jeg laget et lite decabit lib sånn at jeg kan ta det i python (ligger vedlagt):
```
   33 1101100100 ++-++--+-- 104 h
  547 0100111010 -+--+++-+- 101 e
  794 1100010110 ++---+-++- 108 l
 1379 1011001010 +-++--+-+- 115 s
 1459 0100111010 -+--+++-+- 101 e
 1886 0100101110 -+--+-+++-  99 c
 2688 0101001110 -+-+--+++- 116 t
 2878 1010110010 +-+-++--+- 102 f
 3626 0011111000 --+++++--- 123 {
 4022 0001110110 ---+++-++-  98 b
 4535 1011001100 +-++--++-- 105 i
 4855 1100010110 ++---+-++- 108 l
 5279 1000111010 +---+++-+- 100 d
 5668 0100111010 -+--+++-+- 101 e
 6519 0101110010 -+-+++--+-  95 _
 6769 1011001010 +-++--+-+- 115 s
 7541 0101001110 -+-+--+++- 116 t
 8022 0100111010 -+--+++-+- 101 e
 8258 1001101100 +--++-++-- 103 g
 9027 1100100110 ++--+--++-  48 0
 9628 0101110010 -+-+++--+-  95 _
10549 1000111010 +---+++-+- 100 d
11475 1110100100 +++-+--+-- 111 o
11727 1110100010 +++-+---+- 110 n
12563 0100111010 -+--+++-+- 101 e
12715 0101110010 -+-+++--+-  95 _
12867 1010001110 +-+---+++- 114 r
13732 1001010110 +--+-+-++-  49 1
14254 1001101100 +--++-++-- 103 g
14523 1101100100 ++-++--+-- 104 h
15268 0101001110 -+-+--+++- 116 t
15570 0101110010 -+-+++--+-  95 _
16512 0101110010 -+-+++--+-  95 _
17367 1110100010 +++-+---+- 110 n
17635 1110100100 +++-+--+-- 111 o
17684 0101110010 -+-+++--+-  95 _
18292 1101011000 ++-+-++--- 112 p
18528 0111010100 -+++-+-+-- 117 u
19227 1110100010 +++-+---+- 110 n
19581 0101110010 -+-+++--+-  95 _
20398 1011001100 +-++--++-- 105 i
20878 1110100010 +++-+---+- 110 n
21069 0101001110 -+-+--+++- 116 t
21395 0100111010 -+--+++-+- 101 e
21771 1110100010 +++-+---+- 110 n
22063 1000111010 +---+++-+- 100 d
22587 1101100010 ++-++---+-  51 3
22653 1000111010 +---+++-+- 100 d
23387 0101110010 -+-+++--+-  95 _
24296 0110100110 -++-+--++-  58 :
25009 0101010110 -+-+-+-++-  41 )
25721 0110100110 -++-+--++-  58 :
26648 0101010110 -+-+-+-++-  41 )
26889 0000111110 ----+++++- 125 }
```

helsectf{bilde_steg0_done_r1ght__no_pun_intend3d_:):)}
