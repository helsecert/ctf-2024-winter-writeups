import decabit

flag = open("flag.txt","rb").read()
print("flag", flag)
flag_ct = decabit.encode_string(flag)
print("decabit", flag_ct)
flagbits = decabit.encode_string_as_bits(flag)
print(flagbits)

from PIL import Image
img = Image.open("original_image.png")
im = img.load()
w,h = img.size
bitspace = w*h*3

"""
encoder bits som LSB på alle farger.
nuller ut alle andre LSB
legger inn et hint i bunn:
"""
import base64
from Crypto.Util.number import *
hint = base64.b64encode(b"https://de.wikipedia.org/wiki/Decabit") # b'aHR0cHM6Ly9kZS53aWtpcGVkaWEub3JnL3dpa2kvRGVjYWJpdA=='
hintbits = bin(bytes_to_long(hint))[2:] + "0"*(8*24) # litt rom i bunn
bits = flagbits + "0"*(bitspace - len(flagbits) - len(hintbits)) + hintbits
assert len(bits) == bitspace

# encode bits onto LSB
i = 0
for y in range(h):
    for x in range(w):
        p = list(im[x,y])
        im[x,y] = tuple([ (p[rgb] & ~1) | int(bits[i+rgb]) for rgb in range(3)])
        i += 3

img.save("zellweger.png")

# solve
img = Image.open("zellweger.png")
im = img.load()
w,h = img.size
bitspace = w*h*3
bits = ""
for y in range(h):
    for x in range(w):
        p = im[x,y]
        for rgb in range(3):
            bits += str(p[rgb]&1)
#print(bits)
assert bits[0:len(flagbits)] == flagbits
assert bits[-len(hintbits):] == hintbits
hint = int(bits[-len(hintbits):], 2)
print(base64.b64decode(long_to_bytes(hint).replace(b"\x00",b"")))

# les av datastrømmen
PULSE_BIT = "1"
PAUSE_BIT = "0"
parts = []
i = bits.find(PULSE_BIT, 0) # find start impulse
myflag = ""
while i < len(bits):
    part = bits[i+1:i+11]
    assert len(part) == 10        
    try:
        ch = decabit.decode_bits(part)
        myflag += chr(ch)
    except:
        break # hint part
    print(i, part, ch, chr(ch))
    i = bits.find(PULSE_BIT, i+11) # find next start impulse    
print("\n\n", myflag)