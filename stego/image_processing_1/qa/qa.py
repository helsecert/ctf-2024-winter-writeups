"""
kopierte ut det som var oppgitt i oppgaven
og kjøret den i en matplotlib.plt.imshow(spectrum)
"""

from PIL import Image
import numpy
img = Image.open("../src/ekorn.png")
img_data = numpy.asarray(img)

# kopi fra oppgaven
ft = numpy.fft.fft2(img_data)
fshift = numpy.fft.fftshift(ft)
spectrum = numpy.log(numpy.abs(fshift))
#print(spectrum)

import matplotlib.pyplot as plt
plt.imshow(spectrum)
plt.show()
