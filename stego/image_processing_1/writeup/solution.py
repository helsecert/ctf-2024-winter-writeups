import numpy
import matplotlib.pyplot as plt
from PIL import Image
import sys


img = Image.open(sys.argv[1])
data = numpy.asarray(img)

plt.subplot(121)
plt.imshow(data, cmap='gray')

# vi tar 2d fast (discrete) fourier transform av bildet
ft = numpy.fft.fft2(data)
# plasser origo i midten av plottet
fshift = numpy.fft.fftshift(ft)
# fourier-transformasjonen er egentlig kompleks
# vi kan visualisere spektrumet ved å ta normen (absoluttverdien)
# vi tar også logaritmen for å visualisere det bedre (ellers så blir alt så likt)
spectrum = numpy.log(numpy.abs(fshift))
plt.subplot(122)
plt.imshow(spectrum, cmap='gray')
plt.show()

# plottet viser flagget helsectf{krokodille}
