"gratis" flagg.

Klipp og lim det som står i oppgaven i et python console og man får ut flagget:

```python
>>> from pwn import *
>>> io = remote("helsectf2024-2da207d37b091b1b4dff-remote.chals.io", 443, ssl=True)
[x] Opening connection to helsectf2024-2da207d37b091b1b4dff-remote.chals.io on port 443
[x] Opening connection to helsectf2024-2da207d37b091b1b4dff-remote.chals.io on port 443: Trying 143.244.222.116
[+] Opening connection to helsectf2024-2da207d37b091b1b4dff-remote.chals.io on port 443: Done
>>> io.interactive()
[*] Switching to interactive mode
Her har du et gratis flagg: helsectf{remote_flag_er_best,_ingen_protest!}

```