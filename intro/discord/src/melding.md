@everyone HelseCTF 2024 starter kl 12 i dag.

- Alle oppgavene åpnes samtidig: ingen skjulte oppgaver eller oppgaver som dukker opp etter at man har løst en annen oppgave.
- Det er ikke planlagt å gi hint for noen av oppgavene, men det er mulig at vi har undervurdert eller oversett vanskelighetsgraden, hintene i oppgaveteksten, eller at det er direkte feil. Dette blir vurdert fortløpende. Det vil ikke bli gitt hint eller ytterligere informasjon via direkte chat.
- Hvis det blir lagt ut hint for en oppgave, vil dette bli kunngjort i god tid (på Discord + CTFd notification).

Lykke til! `helsectf{men_det_er_jo_lenge_til_Påske!}`