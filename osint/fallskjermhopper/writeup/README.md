De fleste fant kameraet på mfl.no/webcam.html ganske fort.

Ved å lete opp det samme kameraet på f.eks. Windy (windy.com) kan man også få se en 24 timers timelapse med bilde fra hver time. Dette kan hjelpe med å få riktige lysforhold uavhengig av tid på døgnet.
