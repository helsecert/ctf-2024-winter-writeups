# writeup state_of_go
Denne _kan_ løses med chatgpt hvis den instrueres nok. ChatGPT foreslår:
  
  To achieve the desired behavior by changing only one byte, you can modify the assignment within the for loop. Change the line:  
    n := t(n)

  to:
    n = t(n) // Change 'n := t(n)' to 'n = t(n)'

ChatGPT forklarer også hva som er problemet:
  The issue with the original code is related to how Go handles function parameters and variable scope.

  The problem lies within the loop's scope. You are shadowing the outer n variable with a new n variable inside the loop body. This means that the variable n inside the loop is a new variable that has the same name as the outer n, but it is a different variable with its own scope.

  Here's what happens:

  1. The loop starts with n from the outer scope, initialized as {string(flag), -1}.
  2. Inside the loop, you have n := t(n). This creates a new n variable inside the loop's scope, and it shadows the outer n. Any changes made to this new n variable inside the loop do not affect the outer n.
  3. Consequently, each time you call t(n) inside the loop, it modifies the new n variable, not the outer n. This results in the i field of the new n being incremented each time, but the outer n remains unchanged.

  To fix this issue, you need to ensure that the modifications made to n inside the loop affect the outer n. This can be achieved by reassigning the result of t(n) to the outer n with n = t(n) instead of creating a new variable with the := assignment operator. This change ensures that the outer n is modified in each iteration of the loop, and you correctly iterate through the characters of the flag string.

# Flagg
Solve er derfor å erstatte ":=" til " =" altså bytte ut ":" med et mellomrom. Input som gir flagg:
  299
  20

result:helsectf{redeclaring_a_Go_variable_can_shadow_another!}
