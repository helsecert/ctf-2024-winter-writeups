from Crypto.Util.number import inverse, bytes_to_long, long_to_bytes
from pwn import *

oppgavenavn="stateofgo"
io = remote(f"helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io", 443, ssl=True, level="DEBUG")
io.sendline(b"299\n20")
io.recvuntil(b"result:")
io.recvuntil(b"result:")
flag = io.recvline().decode().strip()
print(flag)
