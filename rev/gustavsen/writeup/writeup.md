Det har lite å si om filen er strippet eller ikke da det kun er xor funksjonen i filen som er lokal. Øvrige er libc kall. Vi starter med å kopiere filen før vi evt endrer på den med radare:
```
cp gustavsen gustavsen.ikkestrip
```

Fyrer opp radare2 på en kopi av binarien `r2 -d ./gustavsen.ikkestrip AAAA`

```
[0x00001200]> aaaa
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Finding and parsing C++ vtables (avrr)
[x] Type matching analysis for all functions (aaft)
[x] Propagate noreturn information (aanr)
[x] Integrate dwarf function information.
[x] Finding function preludes
[x] Enable constraint types analysis for variables
[0x00001200]> pdf
            ;-- section..text:
            ;-- _start:
            ;-- rip:
┌ 37: entry0 (int64_t arg3);
│           ; arg int64_t arg3 @ rdx
│           0x00001200      f30f1efa       endbr6                      ; [16] -r-x section size 1177 named .text
│           0x00001204      31ed           ebp = 0
│           0x00001206      4989d1         r9 = rdx                    ; arg3
│           0x00001209      5e             rsi = pop  ()
│           0x0000120a      4889e2         rdx = rsp
│           0x0000120d      4883e4f0       rsp &= 0xfffffffffffffff0
│           0x00001211      50             push  (rax)
│           0x00001212      54             push  (rsp)
│           0x00001213      4531c0         r8d = 0
│           0x00001216      31c9           ecx = 0
│           0x00001218      488d3d5e0100.  rdi = rip + dbg.main        ; 0x137d
└           0x0000121f      ff15b32d0000   qword [reloc.__libc_start_main]  () ; [0x3fd8:8]=0
```

Vi ser at vi sitter i entry0 (_start). Vi kan liste alle funksjoner og ser spesielt `dbg.xor` og `dbg.main`.
```
[0x00001200]> afl
0x00001200    1 37           entry0
0x00001230    4 41   -> 34   sym.deregister_tm_clones
0x00001260    4 57   -> 51   sym.register_tm_clones
0x000012a0    5 57   -> 54   sym.__do_global_dtors_aux
0x00001110    1 11           fcn.00001110
0x000012e0    1 9            entry.init0
0x0000169c    1 13           sym._fini
0x000012e9    4 148          dbg.xor
0x0000137d   19 796          dbg.main
0x00001000    3 27           sym._init
0x00001120    1 11           sym.imp.free
0x00001130    1 11           sym.imp.gelf_getshdr
0x00001140    1 11           sym.imp.elf_version
0x00001150    1 11           sym.imp.puts
0x00001160    1 11           sym.imp.close
0x00001170    1 11           sym.imp.fprintf
0x00001180    1 11           sym.imp.elf_end
0x00001190    1 11           sym.imp.elf_errmsg
0x000011a0    1 11           sym.imp.malloc
0x000011b0    1 11           sym.imp.elf_begin
0x000011c0    1 11           sym.imp.elf_nextscn
0x000011d0    1 11           sym.imp.open
0x000011e0    1 11           sym.imp.perror
0x000011f0    1 11           sym.imp.pread
0x00001030    2 31   -> 28   fcn.00001030
0x00001040    1 15           fcn.00001040
0x00001050    1 15           fcn.00001050
0x00001060    1 15           fcn.00001060
0x00001070    1 15           fcn.00001070
0x00001080    1 15           fcn.00001080
0x00001090    1 15           fcn.00001090
0x000010a0    1 15           fcn.000010a0
0x000010b0    1 15           fcn.000010b0
0x000010c0    1 15           fcn.000010c0
0x000010d0    1 15           fcn.000010d0
0x000010e0    1 15           fcn.000010e0
0x000010f0    1 15           fcn.000010f0
0x00001100    1 15           fcn.00001100
```

Starte virtual mode: vvc, skip over (f8) og skip into (f7)
Breakpoint: db <adresse>
Run: dc

# main
Overordnet så leser filen sine egne section headers ut fra fila.


Litt renaming i r2:
```
afvn fd var_14h
afvn section_data var_30h
afvn shdr var_80h
afvn elf var_20h
afvn scn var_8h
afvn key var_84h
```

Flyt:
- breaker før elf_version sjekkes `db 0x000013ac; dc`

- filedescriptor `fd` blir satt til seg selv (leser sin egen binærfil) ved å kalle libc open på sin egen path:
```
│      │    0x000013fb      e8d0fdffff     sym.imp.open  ()            ; int open(const char *path, int oflag)

[0x5638c35f13d0]> px @ rdi
- offset -      3D3E 3F40 4142 4344 4546 4748 494A 4B4C  DEF0123456789ABC
0x7fff54c7f63d  2f68 6f6d 652f 6d79 7573 6572 2f63 7466  /home/myuser/ctf
0x7fff54c7f64d  2f68 656c 7365 6374 6632 342f 6775 7374  /helsectf24/gust
0x7fff54c7f65d  6176 7365 6e2e 696b 6b65 7374 7269 7000  avsen.ikkestrip.
```

- section index starter på 0:
```
│    │││└─> 0x00001480      48c745f80000.  qword [scn] = 0
```

- arg2 til main() blir så lest inn i en variabel "target_section_name"
```
│    │││    0x00001488      488b8560ffff.  rax = qword [var_a0h]
│    │││    0x0000148f      488b4008       rax = qword [rax + 8]
│    │││    0x00001493      488945d8       qword [target_section_name] = rax
```

- fd blir så lest med `elf_begin`, før den så begynner å lese alle section indexes med `elf_nextscn`
```
│     ││└─> 0x00001422      8b45ec         eax = dword [fd]
│     ││    0x00001425      ba00000000     edx = 0
│     ││    0x0000142a      be01000000     esi = 1
│     ││    0x0000142f      89c7           edi = eax
│     ││    0x00001431      e87afdffff     sym.imp.elf_begin  ()
│     ││    0x00001436      488945e0       qword [elf] = rax
│     ││    0x0000143a      48837de000     var = qword [elf] - 0


│ ╎│││└─> 0x00001638      488b55f8       rdx = qword [scn]
│ │ ╎│││    0x0000163c      488b45e0       rax = qword [elf]
│ │ ╎│││    0x00001640      4889d6         rsi = rdx
│ │ ╎│││    0x00001643      4889c7         rdi = rax
│ │ ╎│││    0x00001646      e875fbffff     sym.imp.elf_nextscn  ()
```

- hver section blir så lest med `gelf_getshdr`
```
│   ┌─────> 0x0000149c      488d5580       rdx = shdr
│   ╎││││   0x000014a0      488b45f8       rax = qword [scn]
│   ╎││││   0x000014a4      4889d6         rsi = rdx
│   ╎││││   0x000014a7      4889c7         rdi = rax
│   ╎││││   0x000014aa      e881fcffff     sym.imp.gelf_getshdr  ()
```

- vi allokerer 0x1c (28 bytes) `size` til et data buffer "section_data":
```
│ │└──────> 0x00001503      488b45a0       rax = qword [size]
│ │ ╎││││   0x00001507      4889c7         rdi = rax                   ; size_t size
│ │ ╎││││   0x0000150a      e891fcffff     sym.imp.malloc  ()          ;  void *malloc(size_t size)
│ │ ╎││││   0x0000150f      488945d0       qword [section_data] = rax
│ │ ╎││││   0x00001513      48837dd000     var = qword [section_data] - 0
```

- før vi skriver til bufferet med `pread` fra `fd` i lengde `size`. var_68h = 0x318 (rcx) på første gjennomføring.
```
│ │└──────> 0x00001549      488b4598       rax = qword [var_68h]
│ │ ╎││││   0x0000154d      4889c1         rcx = rax
│ │ ╎││││   0x00001550      488b55a0       rdx = qword [size]
│ │ ╎││││   0x00001554      488b75d0       rsi = qword [section_data]
│ │ ╎││││   0x00001558      8b45ec         eax = dword [fd]
│ │ ╎││││   0x0000155b      89c7           edi = eax
│ │ ╎││││   0x0000155d      e88efcffff     sym.imp.pread  ()
│ │ ╎││││   0x00001562      488b55a0       rdx = qword [size]
│ │ ╎││││   0x00001566      4839d0         var = rax - rdx
```

Først section header inneholder dette (;-- rip: 0x00001562)
```
[0x5598ae0ba549]> px @ rsi
- offset -      3031 3233 3435 3637 3839 3A3B 3C3D 3E3F  0123456789ABCDEF
0x5598aebefd30  2f6c 6962 3634 2f6c 642d 6c69 6e75 782d  /lib64/ld-linux-
0x5598aebefd40  7838 362d 3634 2e73 6f2e 3200 0000 0000  x86-64.so.2.....
```

- en "magic" 0xeefeaafa lastes inn (little endian) og sjekkes mot uint32 fra section_data[0:4]. hvis de ikke er lik, hopper man til neste scan. vi setter en break på `0x000015b8` som er instr etter sjekken.
```
│ │└──────> 0x000015a6      c745ccfaaafe.  dword [magic] = 0xeefeaafa
│ │ ╎││││   0x000015ad      488b45d0       rax = qword [section_data]
│ │ ╎││││   0x000015b1      8b00           eax = dword [rax]
│ │ ╎││││   0x000015b3      3945cc         var = dword [magic] - eax
``
```

- _hvis_ section_data starter med "faaafeee" (big) så skipper vi uint32 forbi magic, så henter vi ut 4bytes som en XOR key og kjører XOR på resten av bufferet.

```
[0x5598ae0ba5bc]> px 64 @ rax
- offset -      6061 6263 6465 6667 6869 6A6B 6C6D 6E6F  0123456789ABCDEF
0x5598aebefd60  faaa feee 1234 5678 5651 7612 7d56 341d  .....4VxVQv.}V4.
0x5598aebefd70  6014 3f58 7451 3f14 3247 3313 615e 3916  `.?XtQ?.2G3.a^9.
0x5598aebefd80  3e14 3e0a 3c14 110d 6140 370e 6151 3859  >.>.<...a@7.aQ8Y
0x5598aebefd90  0000 0000 0000 0000 0102 0000 0000 0000  ................
[0x5598ae0ba59c]> dr rax
rax = 0x78563412            <- xorkey 
[0x5598ae0ba59c]> dr rax
0x00000030                  <- size
[0x5598ae0ba59c]> dr rcx
0x5598aebefd68              <- adresse til start på buffer som skal xores
```


# feilmeldingen som kommer ut
Vi kan nå lese av dette i python

```
from pwn import xor
key = bytes.fromhex("1234 5678".replace(" ",""))
data = bytes.fromhex("5651 7612 7d56 341d 6014 3f58 7451 3f14 3247 3313 615e 3916 3e14 3e0a 3c14 110d 6140 370e 6151 3859".replace(" ",""))
xor(data,key)
```

# b'De jobber i feil seksjon, hr. Gustavsen!'

# patch
Vi kan patche binarien slik at den fortsetter å lete etter magic segments (alternativt gjør dette i en egen C kode). Vi patcher en jump fra `0x0000160f` slik at den lander i `0x00001638`.
hex(0x1638 - 0x160f - 2) = 0x27 => vi patcher til `eb 27` som vil hoppe over 41 instruksjoner.

Vi kan også bruke radare2 til å regne ut dette for oss og fylle resten med NOP
```
[0x00001200]> s 0x0000160f
[0x0000160f]> pd 10
            0x0000160f      488b45e0       rax = qword [rbp - 0x20]    ; fcntl.h:83 # define S_IFBLK __S_IFBLK
            0x00001613      4889c7         rdi = rax
            0x00001616      e865fbffff     sym.imp.elf_end  ()
            0x0000161b      8b45ec         eax = dword [rbp - 0x14]    ; fcntl.h:84 # define S_IFREG __S_IFREG
            0x0000161e      89c7           edi = eax
            0x00001620      e83bfbffff     sym.imp.close  ()
            0x00001625      b800000000     eax = 0                     ; fcntl.h:85 # ifdef __S_IFIFO
        ┌─< 0x0000162a      eb6b           goto loc_0x1697
        │   0x0000162c      488b45d0       rax = qword [rbp - 0x30]    ; fcntl.h:88 # ifdef __S_IFLNK
        │   0x00001630      4889c7         rdi = rax
[0x0000160f]> wai jmp 0x00001638
INFO: Written 2 byte(s) ( jmp 0x00001638) = wx eb27 @ 0x0000160f
[0x0000160f]> pd 3 @ 0x160f
        ┌─< 0x0000160f      eb27           goto loc_0x1638             ; fcntl.h:83 # define S_IFBLK __S_IFBLK
        │   0x00001611      90             n
        │   0x00001612      90             n
[0x0000160f]> w
```

# objdump
Nå som vi vet hva som er gjort kan vi også finne dette statisk med objdump og kjøre python.
```
$ objdump -s gustavsen.ikkestrip
...
Contents of section .pewpew:
 0000 faaafeee 12345678 56517612 7d56341d  .....4VxVQv.}V4.
 0010 60143f58 74513f14 32473313 615e3916  `.?XtQ?.2G3.a^9.
 0020 3e143e0a 3c14110d 6140370e 61513859  >.>.<...a@7.aQ8Y
Contents of section .symdata:
 0000 faaafeee f376d90a 9b13b579 9615ad6c  .....v.....y...l
 0010 8813be55 96048663 ac13b064 ac05bc61  ...U...c...d...a
 0020 801cb664 ac1eab55 b403aa7e 9200aa6f  ...d...U...~...o
 0030 9d0b    
```

```
from pwn import xor
key = bytes.fromhex("f376d90a")
data = bytes.fromhex("9b13b579 9615ad6c 8813be55 96048663 ac13b064 ac05bc61 801cb664 ac1eab55 b403aa7e 9200aa6f 9d0b".replace(" ",""))
xor(data,key)
```

# Flag
```
$ ./gustavsen.ikkestrip
De jobber i feil seksjon, hr. Gustavsen!
helsectf{eg_er_i_ein_seksjon_hr_Gustavsen}
Section not found: (null)
```

```
[0x5598ae0ba5b3]> px @ rax
- offset -      6061 6263 6465 6667 6869 6A6B 6C6D 6E6F  0123456789ABCDEF
0x5598aebefd60  6865 6c73 6563 7466 7b65 675f 6572 5f69  helsectf{eg_er_i
0x5598aebefd70  5f65 696e 5f73 656b 736a 6f6e 5f68 725f  _ein_seksjon_hr_
0x5598aebefd80  4775 7374 6176 7365 6e7d 000e 6151 3859  Gustavsen}..aQ8Y
```


# main
```
[0x000012e9]> s dbg.main
[0x0000137d]> pdf
            ;-- main:
            ; DATA XREF from entry0 @ 0x1218
┌ 796: int dbg.main (int argc, char **argv);
│           ; var char **var_a0h @ rbp-0xa0
│           ; var int64_t var_94h @ rbp-0x94
│           ; var uint32_t key @ rbp-0x84
│           ; var GElf_Shdr shdr @ rbp-0x80
│           ; var int64_t var_68h @ rbp-0x68
│           ; var size_t size @ rbp-0x60
│           ; var char *data @ rbp-0x40
│           ; var uint32_t magic @ rbp-0x34
│           ; var char *section_data @ rbp-0x30
│           ; var char const *target_section_name @ rbp-0x28
│           ; var Elf *elf @ rbp-0x20
│           ; var int fd @ rbp-0x14
│           ; var char const *elf_file @ rbp-0x10
│           ; var Elf_Scn *scn @ rbp-0x8
│           ; arg int argc @ rdi
│           ; arg char **argv @ rsi
│           0x0000137d      f30f1efa       endbr6                      ; int main(int argc,char ** argv);
│           0x00001381      55             push  (rbp)
│           0x00001382      4889e5         rbp = rsp
│           0x00001385      4881eca00000.  rsp -= 0xa0
│           0x0000138c      89bd6cffffff   dword [var_94h] = edi       ; argc
│           0x00001392      4889b560ffff.  qword [var_a0h] = rsi       ; argv
│           0x00001399      488b8560ffff.  rax = qword [var_a0h]
│           0x000013a0      488b00         rax = qword [rax]
│           0x000013a3      488945f0       qword [elf_file] = rax
│           0x000013a7      bf01000000     edi = 1
│           0x000013ac      e88ffdffff     sym.imp.elf_version  ()
│           0x000013b1      85c0           var = eax & eax
│       ┌─< 0x000013b3      7535           if  (var) goto loc_0x13ea
│       │   0x000013b5      bfffffffff     edi = 0xffffffff            ; -1
│       │   0x000013ba      e8d1fdffff     sym.imp.elf_errmsg  ()
│       │   0x000013bf      4889c2         rdx = rax                   ;   ...
│       │   0x000013c2      488b05572c00.  rax = qword [obj.stderr]    ; obj.stderr_GLIBC_2.2.5
│       │                                                              ; [0x4020:8]=0
│       │   0x000013c9      488d0d380c00.  rcx = rip + str.ELF_library_initialization_failed:__s_n ; 0x2008 ; "ELF library initialization failed: %s\n"
│       │   0x000013d0      4889ce         rsi = rcx                   ; const char *format
│       │   0x000013d3      4889c7         rdi = rax                   ; FILE *stream
│       │   0x000013d6      b800000000     eax = 0
│       │   0x000013db      e890fdffff     sym.imp.fprintf  ()         ; int fprintf(FILE *stream, const char *format,   ...)
│       │   0x000013e0      b801000000     eax = 1
│      ┌──< 0x000013e5      e9ad020000     goto loc_0x1697
│      ││   ; CODE XREF from dbg.main @ 0x13b3
│      │└─> 0x000013ea      488b45f0       rax = qword [elf_file]
│      │    0x000013ee      be00000000     esi = 0                     ; int oflag
│      │    0x000013f3      4889c7         rdi = rax                   ; const char *path
│      │    0x000013f6      b800000000     eax = 0
│      │    0x000013fb      e8d0fdffff     sym.imp.open  ()            ; int open(const char *path, int oflag)
│      │    0x00001400      8945ec         dword [fd] = eax
│      │    0x00001403      837dec00       var = dword [fd] - 0
│      │┌─< 0x00001407      7919           jns 0x1422
│      ││   0x00001409      488d051f0c00.  rax = rip + str.open        ; 0x202f ; "open"
│      ││   0x00001410      4889c7         rdi = rax                   ; const char *s
│      ││   0x00001413      e8c8fdffff     sym.imp.perror  ()          ; void perror(const char *s)
│      ││   0x00001418      b801000000     eax = 1
│     ┌───< 0x0000141d      e975020000     goto loc_0x1697
│     │││   ; CODE XREF from dbg.main @ 0x1407
│     ││└─> 0x00001422      8b45ec         eax = dword [fd]
│     ││    0x00001425      ba00000000     edx = 0
│     ││    0x0000142a      be01000000     esi = 1
│     ││    0x0000142f      89c7           edi = eax
│     ││    0x00001431      e87afdffff     sym.imp.elf_begin  ()
│     ││    0x00001436      488945e0       qword [elf] = rax
│     ││    0x0000143a      48837de000     var = qword [elf] - 0
│     ││┌─< 0x0000143f      753f           if  (var) goto loc_0x1480
│     │││   0x00001441      bfffffffff     edi = 0xffffffff            ; -1
│     │││   0x00001446      e845fdffff     sym.imp.elf_errmsg  ()
│     │││   0x0000144b      4889c2         rdx = rax                   ;   ...
│     │││   0x0000144e      488b05cb2b00.  rax = qword [obj.stderr]    ; obj.stderr_GLIBC_2.2.5
│     │││                                                              ; [0x4020:8]=0
│     │││   0x00001455      488d0dd80b00.  rcx = rip + str.elf_begin:__s_n ; 0x2034 ; "elf_begin: %s\n"
│     │││   0x0000145c      4889ce         rsi = rcx                   ; const char *format
│     │││   0x0000145f      4889c7         rdi = rax                   ; FILE *stream
│     │││   0x00001462      b800000000     eax = 0
│     │││   0x00001467      e804fdffff     sym.imp.fprintf  ()         ; int fprintf(FILE *stream, const char *format,   ...)
│     │││   0x0000146c      8b45ec         eax = dword [fd]
│     │││   0x0000146f      89c7           edi = eax                   ; int fildes
│     │││   0x00001471      e8eafcffff     sym.imp.close  ()           ; int close(int fildes)
│     │││   0x00001476      b801000000     eax = 1
│    ┌────< 0x0000147b      e917020000     goto loc_0x1697
│    ││││   ; CODE XREF from dbg.main @ 0x143f
│    │││└─> 0x00001480      48c745f80000.  qword [scn] = 0
│    │││    0x00001488      488b8560ffff.  rax = qword [var_a0h]
│    │││    0x0000148f      488b4008       rax = qword [rax + 8]
│    │││    0x00001493      488945d8       qword [target_section_name] = rax
│    │││┌─< 0x00001497      e99c010000     goto loc_0x1638
│    ││││   ; CODE XREF from dbg.main @ 0x1654
│   ┌─────> 0x0000149c      488d5580       rdx = shdr
│   ╎││││   0x000014a0      488b45f8       rax = qword [scn]
│   ╎││││   0x000014a4      4889d6         rsi = rdx
│   ╎││││   0x000014a7      4889c7         rdi = rax
│   ╎││││   0x000014aa      e881fcffff     sym.imp.gelf_getshdr  ()
│   ╎││││   0x000014af      488d5580       rdx = shdr
│   ╎││││   0x000014b3      4839d0         var = rax - rdx
│  ┌──────< 0x000014b6      744b           if  (!var) goto loc_0x1503
│  │╎││││   0x000014b8      bfffffffff     edi = 0xffffffff            ; -1
│  │╎││││   0x000014bd      e8cefcffff     sym.imp.elf_errmsg  ()
│  │╎││││   0x000014c2      4889c2         rdx = rax                   ;   ...
│  │╎││││   0x000014c5      488b05542b00.  rax = qword [obj.stderr]    ; obj.stderr_GLIBC_2.2.5
│  │╎││││                                                              ; [0x4020:8]=0
│  │╎││││   0x000014cc      488d0d700b00.  rcx = rip + str.gelf_getshdr___failed:__s_n ; 0x2043 ; "gelf_getshdr() failed: %s\n"
│  │╎││││   0x000014d3      4889ce         rsi = rcx                   ; const char *format
│  │╎││││   0x000014d6      4889c7         rdi = rax                   ; FILE *stream
│  │╎││││   0x000014d9      b800000000     eax = 0
│  │╎││││   0x000014de      e88dfcffff     sym.imp.fprintf  ()         ; int fprintf(FILE *stream, const char *format,   ...)
│  │╎││││   0x000014e3      488b45e0       rax = qword [elf]
│  │╎││││   0x000014e7      4889c7         rdi = rax
│  │╎││││   0x000014ea      e891fcffff     sym.imp.elf_end  ()
│  │╎││││   0x000014ef      8b45ec         eax = dword [fd]
│  │╎││││   0x000014f2      89c7           edi = eax                   ; int fildes
│  │╎││││   0x000014f4      e867fcffff     sym.imp.close  ()           ; int close(int fildes)
│  │╎││││   0x000014f9      b801000000     eax = 1
│ ┌───────< 0x000014fe      e994010000     goto loc_0x1697
│ ││╎││││   ; CODE XREF from dbg.main @ 0x14b6
│ │└──────> 0x00001503      488b45a0       rax = qword [size]
│ │ ╎││││   0x00001507      4889c7         rdi = rax                   ; size_t size
│ │ ╎││││   0x0000150a      e891fcffff     sym.imp.malloc  ()          ;  void *malloc(size_t size)
│ │ ╎││││   0x0000150f      488945d0       qword [section_data] = rax
│ │ ╎││││   0x00001513      48837dd000     var = qword [section_data] - 0
│ │┌──────< 0x00001518      752f           if  (var) goto loc_0x1549
│ ││╎││││   0x0000151a      488d053d0b00.  rax = rip + str.malloc      ; 0x205e ; "malloc"
│ ││╎││││   0x00001521      4889c7         rdi = rax                   ; const char *s
│ ││╎││││   0x00001524      e8b7fcffff     sym.imp.perror  ()          ; void perror(const char *s)
│ ││╎││││   0x00001529      488b45e0       rax = qword [elf]
│ ││╎││││   0x0000152d      4889c7         rdi = rax
│ ││╎││││   0x00001530      e84bfcffff     sym.imp.elf_end  ()
│ ││╎││││   0x00001535      8b45ec         eax = dword [fd]
│ ││╎││││   0x00001538      89c7           edi = eax                   ; int fildes
│ ││╎││││   0x0000153a      e821fcffff     sym.imp.close  ()           ; int close(int fildes)
│ ││╎││││   0x0000153f      b801000000     eax = 1
│ ────────< 0x00001544      e94e010000     goto loc_0x1697
│ ││╎││││   ; CODE XREF from dbg.main @ 0x1518
│ │└──────> 0x00001549      488b4598       rax = qword [var_68h]
│ │ ╎││││   0x0000154d      4889c1         rcx = rax
│ │ ╎││││   0x00001550      488b55a0       rdx = qword [size]
│ │ ╎││││   0x00001554      488b75d0       rsi = qword [section_data]
│ │ ╎││││   0x00001558      8b45ec         eax = dword [fd]
│ │ ╎││││   0x0000155b      89c7           edi = eax
│ │ ╎││││   0x0000155d      e88efcffff     sym.imp.pread  ()
│ │ ╎││││   0x00001562      488b55a0       rdx = qword [size]
│ │ ╎││││   0x00001566      4839d0         var = rax - rdx
│ │┌──────< 0x00001569      743b           if  (!var) goto loc_0x15a6
│ ││╎││││   0x0000156b      488d05f30a00.  rax = rip + str.pread       ; 0x2065 ; "pread"
│ ││╎││││   0x00001572      4889c7         rdi = rax                   ; const char *s
│ ││╎││││   0x00001575      e866fcffff     sym.imp.perror  ()          ; void perror(const char *s)
│ ││╎││││   0x0000157a      488b45d0       rax = qword [section_data]
│ ││╎││││   0x0000157e      4889c7         rdi = rax                   ; void *ptr
│ ││╎││││   0x00001581      e89afbffff     sym.imp.free  ()            ; void free(void *ptr)
│ ││╎││││   0x00001586      488b45e0       rax = qword [elf]
│ ││╎││││   0x0000158a      4889c7         rdi = rax
│ ││╎││││   0x0000158d      e8eefbffff     sym.imp.elf_end  ()
│ ││╎││││   0x00001592      8b45ec         eax = dword [fd]
│ ││╎││││   0x00001595      89c7           edi = eax                   ; int fildes
│ ││╎││││   0x00001597      e8c4fbffff     sym.imp.close  ()           ; int close(int fildes)
│ ││╎││││   0x0000159c      b801000000     eax = 1
│ ────────< 0x000015a1      e9f1000000     goto loc_0x1697
│ ││╎││││   ; CODE XREF from dbg.main @ 0x1569
│ │└──────> 0x000015a6      c745ccfaaafe.  dword [magic] = 0xeefeaafa
│ │ ╎││││   0x000015ad      488b45d0       rax = qword [section_data]
│ │ ╎││││   0x000015b1      8b00           eax = dword [rax]
│ │ ╎││││   0x000015b3      3945cc         var = dword [magic] - eax
│ │┌──────< 0x000015b6      7574           if  (var) goto loc_0x162c
│ ││╎││││   0x000015b8      488b45d0       rax = qword [section_data]
│ ││╎││││   0x000015bc      8b4004         eax = dword [rax + 4]
│ ││╎││││   0x000015bf      89857cffffff   dword [key] = eax
│ ││╎││││   0x000015c5      488b45a0       rax = qword [size]
│ ││╎││││   0x000015c9      488d70f8       rsi = rax - 8               ; int64_t arg2
│ ││╎││││   0x000015cd      488b45d0       rax = qword [section_data]
│ ││╎││││   0x000015d1      488d4808       rcx = rax + 8
│ ││╎││││   0x000015d5      488d857cffff.  rax = key
│ ││╎││││   0x000015dc      4889c2         rdx = rax                   ; int64_t arg3
│ ││╎││││   0x000015df      4889cf         rdi = rcx                   ; int64_t arg1
│ ││╎││││   0x000015e2      e802fdffff     dbg.xor  ()
│ ││╎││││   0x000015e7      488945c0       qword [data] = rax
│ ││╎││││   0x000015eb      488b45c0       rax = qword [data]
│ ││╎││││   0x000015ef      4889c7         rdi = rax                   ; const char *s
│ ││╎││││   0x000015f2      e859fbffff     sym.imp.puts  ()            ; int puts(const char *s)
│ ││╎││││   0x000015f7      488b45d0       rax = qword [section_data]
│ ││╎││││   0x000015fb      4889c7         rdi = rax                   ; void *ptr
│ ││╎││││   0x000015fe      e81dfbffff     sym.imp.free  ()            ; void free(void *ptr)
│ ││╎││││   0x00001603      488b45c0       rax = qword [data]
│ ││╎││││   0x00001607      4889c7         rdi = rax                   ; void *ptr
│ ││╎││││   0x0000160a      e811fbffff     sym.imp.free  ()            ; void free(void *ptr)
│ ││╎││││   0x0000160f      488b45e0       rax = qword [elf]
│ ││╎││││   0x00001613      4889c7         rdi = rax
│ ││╎││││   0x00001616      e865fbffff     sym.imp.elf_end  ()
│ ││╎││││   0x0000161b      8b45ec         eax = dword [fd]
│ ││╎││││   0x0000161e      89c7           edi = eax                   ; int fildes
│ ││╎││││   0x00001620      e83bfbffff     sym.imp.close  ()           ; int close(int fildes)
│ ││╎││││   0x00001625      b800000000     eax = 0
│ ────────< 0x0000162a      eb6b           goto loc_0x1697
│ ││╎││││   ; CODE XREF from dbg.main @ 0x15b6
│ │└──────> 0x0000162c      488b45d0       rax = qword [section_data]
│ │ ╎││││   0x00001630      4889c7         rdi = rax                   ; void *ptr
│ │ ╎││││   0x00001633      e8e8faffff     sym.imp.free  ()            ; void free(void *ptr)
│ │ ╎││││   ; CODE XREF from dbg.main @ 0x1497
│ │ ╎│││└─> 0x00001638      488b55f8       rdx = qword [scn]
│ │ ╎│││    0x0000163c      488b45e0       rax = qword [elf]
│ │ ╎│││    0x00001640      4889d6         rsi = rdx
│ │ ╎│││    0x00001643      4889c7         rdi = rax
│ │ ╎│││    0x00001646      e875fbffff     sym.imp.elf_nextscn  ()
│ │ ╎│││    0x0000164b      488945f8       qword [scn] = rax
│ │ ╎│││    0x0000164f      48837df800     var = qword [scn] - 0
│ │ └─────< 0x00001654      0f8542feffff   if  (var) goto loc_0x149c
│ │  │││    0x0000165a      488b05bf2900.  rax = qword [obj.stderr]    ; obj.stderr_GLIBC_2.2.5
│ │  │││                                                               ; [0x4020:8]=0
│ │  │││    0x00001661      488b55d8       rdx = qword [target_section_name] ;   ...
│ │  │││    0x00001665      488d0dff0900.  rcx = rip + str.Section_not_found:__s_n ; 0x206b ; "Section not found: %s\n"
│ │  │││    0x0000166c      4889ce         rsi = rcx                   ; const char *format
│ │  │││    0x0000166f      4889c7         rdi = rax                   ; FILE *stream
│ │  │││    0x00001672      b800000000     eax = 0
│ │  │││    0x00001677      e8f4faffff     sym.imp.fprintf  ()         ; int fprintf(FILE *stream, const char *format,   ...)
│ │  │││    0x0000167c      488b45e0       rax = qword [elf]
│ │  │││    0x00001680      4889c7         rdi = rax
│ │  │││    0x00001683      e8f8faffff     sym.imp.elf_end  ()
│ │  │││    0x00001688      8b45ec         eax = dword [fd]
│ │  │││    0x0000168b      89c7           edi = eax                   ; int fildes
│ │  │││    0x0000168d      e8cefaffff     sym.imp.close  ()           ; int close(int fildes)
│ │  │││    0x00001692      b801000000     eax = 1
│ │  │││    ; XREFS: CODE 0x000013e5  CODE 0x0000141d  CODE 0x0000147b  CODE 0x000014fe  CODE 0x00001544  CODE 0x000015a1
│ │  │││    ; XREFS: CODE 0x0000162a
│ └──└└└──> 0x00001697      c9             leav
└           0x00001698      c3             re
```

# xor
Vi ser at funksjonen tar inn en peker til data, en lengde og en nøkkel. Dataene blir XOR'et med key[i%4] for ith posisjon.

```
[0x00001200]> s dbg.xor
[0x000012e9]> pdf
            ;-- xor:
            ; CALL XREF from dbg.main @ 0x15e2
┌ 148: dbg.xor (int64_t arg1, size_t arg2, int64_t arg3);
│           ; var char *key @ rbp-0x28
│           ; var size_t len @ rbp-0x20
│           ; var char *data @ rbp-0x18
│           ; var char *result @ rbp-0x10
│           ; var int i @ rbp-0x4
│           ; arg int64_t arg1 @ rdi
│           ; arg size_t arg2 @ rsi
│           ; arg int64_t arg3 @ rdx
│           0x000012e9      f30f1efa       endbr6                      ; char * xor(char * data,size_t len,char * key);
│           0x000012ed      55             push  (rbp)
│           0x000012ee      4889e5         rbp = rsp
│           0x000012f1      4883ec30       rsp -= 0x30
│           0x000012f5      48897de8       qword [data] = rdi          ; arg1
│           0x000012f9      488975e0       qword [len] = rsi           ; arg2
│           0x000012fd      488955d8       qword [key] = rdx           ; arg3
│           0x00001301      488b45e0       rax = qword [len]
│           0x00001305      4883c001       rax += 1
│           0x00001309      4889c7         rdi = rax                   ; size_t size
│           0x0000130c      e88ffeffff     sym.imp.malloc  ()          ;  void *malloc(size_t size)
│           0x00001311      488945f0       qword [result] = rax
│           0x00001315      c745fc000000.  dword [i] = 0
│       ┌─< 0x0000131c      eb40           goto loc_0x135e
│       │   ; CODE XREF from dbg.xor @ 0x1367
│      ┌──> 0x0000131e      8b45fc         eax = dword [i]
│      ╎│   0x00001321      4863d0         rdx = eax
│      ╎│   0x00001324      488b45e8       rax = qword [data]
│      ╎│   0x00001328      4801d0         rax += rdx
│      ╎│   0x0000132b      0fb608         ecx = byte [rax]
│      ╎│   0x0000132e      8b45fc         eax = dword [i]
│      ╎│   0x00001331      99             cd
│      ╎│   0x00001332      c1ea1e         edx >>>= 0x1e
│      ╎│   0x00001335      01d0           eax += edx
│      ╎│   0x00001337      83e003         eax &= 3
│      ╎│   0x0000133a      29d0           eax -= edx
│      ╎│   0x0000133c      4863d0         rdx = eax
│      ╎│   0x0000133f      488b45d8       rax = qword [key]
│      ╎│   0x00001343      4801d0         rax += rdx
│      ╎│   0x00001346      0fb610         edx = byte [rax]
│      ╎│   0x00001349      8b45fc         eax = dword [i]
│      ╎│   0x0000134c      4863f0         rsi = eax
│      ╎│   0x0000134f      488b45f0       rax = qword [result]
│      ╎│   0x00001353      4801f0         rax += rsi
│      ╎│   0x00001356      31ca           edx ^= ecx
│      ╎│   0x00001358      8810           byte [rax] = dl
│      ╎│   0x0000135a      8345fc01       dword [i] += 1
│      ╎│   ; CODE XREF from dbg.xor @ 0x131c
│      ╎└─> 0x0000135e      8b45fc         eax = dword [i]
│      ╎    0x00001361      4898           cdq
│      ╎    0x00001363      483945e0       var = qword [len] - rax
│      └──< 0x00001367      77b5           if  (((unsigned) var) > 0) goto 0x131e
│           0x00001369      488b55f0       rdx = qword [result]
│           0x0000136d      488b45e0       rax = qword [len]
│           0x00001371      4801d0         rax += rdx
│           0x00001374      c60000         byte [rax] = 0
│           0x00001377      488b45f0       rax = qword [result]
│           0x0000137b      c9             leav
└           0x0000137c      c3             re
```