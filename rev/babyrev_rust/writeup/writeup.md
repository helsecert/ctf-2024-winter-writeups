Det er flere veier til flagg her. Vi kan åpne filen i et RE verktøy (ghidra, IDA pro, binaryninja++) og se hva som skjer. Vi ser at det er en stor array som settes bokstaver i ulik rekkefølge (flagget), også kjøres det en "reverse" og en "join". 
Også er det en sjekk i bunn på om vi har riktig flagg eller ikke. Det er også en sjekk som kjører funkjsonen `hint()`... :)

Det ligger 2 hint i fila:

# Hint1
Den ene ligger som en utf-16-le string i section .h_i_n_t i ELF binarien, som er reversert. Dette for å gjøre den litt mindre lesbar, og at den ikke ramler ut med `strings` sånn uten videre. Du kan prøve `strings -el babyrev_rust -n 20` så vil du se den satt sammen.

Intended er å se på sections i ELF fila, i bunn der ligger den som ".h_i_n_t":
```
$ objdump -s babyrev_rust
...

Contents of section .h_i_n_t:
 0000 3f005200 45004700 47005500 42004500  ?.R.E.G.G.U.B.E.
 0010 44002000 6e006500 20006500 6b007500  D. .n.e. .e.k.u.
 0020 72006200 20006e00 61006b00 20006700  r.b. .n.a.k. .g.
 0030 65006a00 20006500 6a006b00 73006e00  e.j. .e.j.k.s.n.
 0040 61006b00 20003a00 6b006e00 69006800  a.k. .:.k.n.i.h.
 0050 74003a00 20003f00 3f002900 28006e00  t.:. .?.?.).(.n.
 0060 69006100 6d002000 65007300 72006500  i.a.m. .e.s.r.e.
 0070 76006500 72002000 e5002000 6e006e00  v.e.r. ... .n.n.
 0080 65002000 e5007000 20007400 65006700  e. ...p. .t.e.g.
 0090 67006100 6c006600 20006500 6e006e00  g.a.l.f. .e.n.n.
 00a0 69006600 2000e500 20006500 7400e500  i.f. ... .e.t...
 00b0 6d002000 65007200 65006c00 6b006e00  m. .e.r.e.l.k.n.
 00c0 65002000 6e006500 20006500 7200e600  e. .n.e. .e.r...
 00d0 76002000 74006500 64002000 6e006100  v. .t.e.d. .n.a.
 00e0 4b00                                 K.

$ objcopy --dump-section .h_i_n_t=hint1.bin babyrev_rust
$ python -c 'print(open("hint1.bin","rb").read().decode("utf-16-le")[::-1])'
Kan det være en enklere måte å finne flagget på enn å reverse main()?? :think: kanskje jeg kan bruke en DEBUGGER?
```

Dette peker mot å bruke en debugger (gdb, radare2++).

# Hint2
Det andre hintet ligger i som en funksjon `hint()` med en rot13 (cæsar) kryptert streng.
```
/* main::hint */
void __rustcall main::hint(void)
{
  undefined local_80 [128];
  core::str::_<impl_str>::split
            (local_80,"hint=Bzwrtonerunqqruhfxrguibeqnawrtoehxreragenpre",0x31,&DAT_00149298,0);
  return;
}
```

Du kan prøve denne på f.eks. https://www.dcode.fr/cipher-identifier og den vil foreslår blant annet "rot-13 cipher": https://www.dcode.fr/rot-13-cipher

Denne gir deg `Omjegbarehaddehuskethvordanjegbrukerentracer` eller `Om jeg bare hadde husket hvordan jeg bruker en tracer`. Dette peker mot strace/ltrace som løsning.


# debugger med breakpoint
Vi kan sette ulike breakpoints, f.eks. etter join og se i minnet. Jeg bruker radare2, setter breakpoint til instruksjonen etter join er kjørt også sjekker jeg pekeren i rsi/rdi:
```
$ r2 -d ./main test

[0x7fd3789ce090]> aaaa
[Invalid address from 0x560acac9275esym. and entry0 (aa)
Invalid address from 0x560acacb5683
Invalid address from 0x560acacb561f
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Finding and parsing C++ vtables (avrr)
[x] Skipping type matching analysis in debugger mode (aaft)
[x] Propagate noreturn information (aanr)
[x] Finding function preludes
[x] Enable constraint types analysis for variables
[0x7fd3789ce090]> s sym.main
[0x560acac8d5d0]> pdf
            ; DATA XREF from entry0 @ 0x560acac87621
┌ 21: int main (int argc, char **argv);
│           ; arg int argc @ rdi
│           ; arg char **argv @ rsi
│           0x560acac8d5d0      50             push  (rax)
│           0x560acac8d5d1      4889f2         rdx = rsi               ; argv
│           0x560acac8d5d4      4863f7         rsi = edi               ; argc
│           0x560acac8d5d7      488d3d92f4ff.  rdi = rip - sym.main::main::h35737df686587ba4 ; 0x560acac8ca70 ; "SH\x81\xec`\x04"
│           0x560acac8d5de      e8fd240000     sym.std::rt::lang_start::hd9c4d0541b6f522d  ()
│           0x560acac8d5e3      59             rcx = pop  ()
└           0x560acac8d5e4      c3             re
[0x560acac8d5d0]> s 0x560acac8ca70
[0x560acac8ca70]> pdf
Do you want to print 546 lines? (y/N) y
            ; DATA XREF from main @ 0x560acac8d5d7
┌ 2682: sym.main::main::h35737df686587ba4 ();
│           ; var int64_t var_fh @ rsp+0xf
│           ; var int64_t var_10h @ rsp+0x10
│           ; var int64_t var_18h @ rsp+0x18
│           ; var int64_t var_20h @ rsp+0x20
│           ; var int64_t var_28h @ rsp+0x2

...

   ││ ╎╎   0x560acac8d3dd      31d2           edx = 0
│   ││ ╎╎   0x560acac8d3df      4189d0         r8d = edx
│   ││ ╎╎   0x560acac8d3e2      488dbc24c003.  rdi = var_3c0h
│   ││ ╎╎   0x560acac8d3ea      488db424d800.  rsi = var_d8h
│   ││ ╎╎   0x560acac8d3f2      ba2a000000     edx = 0x2a              ; '*' ; 42
│   ││ ╎╎   0x560acac8d3f7      e8642d0000     sym.alloc::slice::__impl__T__::join::h2e2e9e530de23482  () ; alloc::slice::_<impl [T]>::join::h2e2e9e530de23482
│   ││┌───< 0x560acac8d3fc      eb00           goto loc_0x560acac8d3fe

...

[0x560acac8ca70]> db 0x560acac8d3fc
[0x560acac8ca70]> dc
hit breakpoint at: 0x560acac8d3fc
[0x560acac8d3fc]> px8 @ rsi
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7fffd41fe0b0  b02c 7bcc 0a56 0000                      .,{..V..
[0x560acac8d3fc]> px8 @ rdi
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7fffd41fe4b0  b02c 7bcc 0a56 0000                      .,{..V..
[0x560acac8d3fc]> pv @ rdi
0x0000560acc7b2cb0
[0x560acac8d3fc]> px @ 0x0000560acc7b2cb0
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x560acc7b2cb0  7d21 6769 746b 6972 5f74 7669 7461 6c65  }!gitkir_tvitale
0x560acc7b2cc0  725f 7265 6d69 725f 7473 7572 5f6b 7361  r_remir_tsur_ksa
0x560acc7b2cd0  727b 6674 6365 736c 6568 0000 0000 0000  r{ftcesleh......
0x560acc7b2ce0  0000 0000 0000 0000 2103 0200 0000 0000  ........!.......
0x560acc7b2cf0  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d00  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d10  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d20  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d30  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d40  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d50  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d60  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d70  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d80  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2d90  0000 0000 0000 0000 0000 0000 0000 0000  ................
0x560acc7b2da0  0000 0000 0000 0000 0000 0000 0000 0000  ................

[0x560acac8d3fc]> pf S @ rsi
0x7fffd41fe0b0 = 0x7fffd41fe0b0 -> 0x560acc7b2cb0 "}!gitkir_tvitaler_remir_tsur_ksar{ftcesleh"
```

Den kan vi snu med python
```python
$ python -c 'print("}!gitkir_tvitaler_remir_tsur_ksar{ftcesleh"[::-1])'
helsectf{rask_rust_rimer_relativt_riktig!}
```

# strace/ltrace
En snarvei på CTFer for enklere binaries er å kjøre den med strace (system calls) eller ltrace (library call).
```
❯ ltrace ./main test
memcpy(0x55667e94aba0, "f", 1)                                                                                                       = 0x55667e94aba0
memcpy(0x55667e94aba1, "", 0)                                                                                                        = 0x55667e94aba1
memcpy(0x55667e94aba1, "a", 1)                                                                                                       = 0x55667e94aba1
memcpy(0x55667e94aba2, "", 0)                                                                                                        = 0x55667e94aba2
memcpy(0x55667e94aba2, "l", 1)                                                                                                       = 0x55667e94aba2
memcpy(0x55667e94aba3, "", 0)                                                                                                        = 0x55667e94aba3
memcpy(0x55667e94aba3, "s", 1)                                                                                                       = 0x55667e94aba3
memcpy(0x55667e94aba4, "", 0)                                                                                                        = 0x55667e94aba4
memcpy(0x55667e94aba4, "e", 1)                                                                                                       = 0x55667e94aba4
memcpy(0x55667e94aba5, "", 0)                                                                                                        = 0x55667e94aba5
memcpy(0x55667e94aba5, "_", 1)                                                                                                       = 0x55667e94aba5
memcpy(0x55667e94aba6, "", 0)                                                                                                        = 0x55667e94aba6
...
memcpy(0x55667e94acd1, "{", 1)                                                                                                       = 0x55667e94acd1
memcpy(0x55667e94acd2, "", 0)                                                                                                        = 0x55667e94acd2
memcpy(0x55667e94acd2, "f", 1)                                                                                                       = 0x55667e94acd2
memcpy(0x55667e94acd3, "", 0)                                                                                                        = 0x55667e94acd3
memcpy(0x55667e94acd3, "t", 1)                                                                                                       = 0x55667e94acd3
memcpy(0x55667e94acd4, "", 0)                                                                                                        = 0x55667e94acd4
memcpy(0x55667e94acd4, "c", 1)                                                                                                       = 0x55667e94acd4
memcpy(0x55667e94acd5, "", 0)                                                                                                        = 0x55667e94acd5
memcpy(0x55667e94acd5, "e", 1)                                                                                                       = 0x55667e94acd5
memcpy(0x55667e94acd6, "", 0)                                                                                                        = 0x55667e94acd6
memcpy(0x55667e94acd6, "s", 1)                                                                                                       = 0x55667e94acd6
memcpy(0x55667e94acd7, "", 0)                                                                                                        = 0x55667e94acd7
memcpy(0x55667e94acd7, "l", 1)                                                                                                       = 0x55667e94acd7
memcpy(0x55667e94acd8, "", 0)                                                                                                        = 0x55667e94acd8
memcpy(0x55667e94acd8, "e", 1)                                                                                                       = 0x55667e94acd8
memcpy(0x55667e94acd9, "", 0)                                                                                                        = 0x55667e94acd9
memcpy(0x55667e94acd9, "h", 1)                                                                                                       = 0x55667e94acd9
De må prøve hardere.
__cxa_finalize(0x55667e1dbe90, 1, 1, 0)                                                                                              = 0

```

I bunn ser vi "h e l s e c t f {" skrevet baklengs. Vi kan bruke litt linux cli magi på å hente ut alle linjene, sette de sammen og snu den.
```
❯ ltrace ./main test 2>&1 | grep ", 1)" | sed -n "s/.*, \"\(.*\)\", .*/\1/p" | tr '\n' ' ' | sed -s 's/ //g'
false_flags_are_not_cool_to_hunt_correctly}!gitkir_tvitaler_remir_tsur_ksar{ftcesleh

❯ ltrace ./main test 2>&1 | grep ", 1)" | sed -n "s/.*, \"\(.*\)\", .*/\1/p" | tr '\n' ' ' | sed -s 's/ //g' | rev
helsectf{rask_rust_rimer_relativt_riktig!}yltcerroc_tnuh_ot_looc_ton_era_sgalf_eslaf[
```


