use std::env;
use std::process;

fn hint() {
   let s = "hint=Bzwrtonerunqqruhfxrguibeqnawrtoehxreragenpre".split("");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("De mangler argumentet. Vennligst prøv på nytt, hvis du søker flagg kan det være lurt å starte med helsectf.");
        process::exit(6517862);
    }
    let input = String::from(&args[1]) ;

    let mut arr = ["f", "a", "l", "s", "e", "_", "f", "l", "a", "g", "s", "_", "a", "r", "e", "_", "n", "o", "t", "_", "c", "o", "o", "l", "_", "t", "o", "_", "h", "u", "n", "t", "_", "c", "o", "r", "r", "e", "c", "t", "l", "y", " ", " "];
    let r2 = arr.join("");
    if r2 == input {
        println!("Dette er et falskt flagg.");
        hint();
    } else {
        arr[0] = "h";
        arr[1] = "e";
        arr[2] = "l";
        arr[3] = "s";
        arr[4] = "e";
        arr[5] = "c";
        arr[6] = "t";
        arr[7] = "f";
        arr[8] = "{";
        arr[15] = "u";
        arr[9] = "r";
        arr[28] = "a";
        arr[22] = "e";
        arr[17] = "t";
        arr[39] = "g";
        arr[24] = "_";
        arr[31] = "v";
        arr[12] = "k";
        arr[14] = "r";
        arr[34] = "r";
        arr[13] = "_";
        arr[27] = "l";
        arr[11] = "s";
        arr[23] = "r";
        arr[29] = "t";
        arr[40] = "!";
        arr[19] = "r";
        arr[18] = "_";
        arr[32] = "t";
        arr[33] = "_";
        arr[37] = "t";
        arr[20] = "i";
        arr[36] = "k";
        arr[35] = "i";
        arr[38] = "i";
        arr[25] = "r";
        arr[30] = "i";
        arr[21] = "m";
        arr[26] = "e";
        arr[10] = "a";
        arr[16] = "s";
        arr[41] = "}";
        arr.reverse();
        let r = arr.join("");

        let z = input.chars().rev().collect::<String>();
        match z == r {
            true => println!("Men, De vet jo flagget!"),
            _ => println!("De må prøve hardere.")
        }    
    }
}