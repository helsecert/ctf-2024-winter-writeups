import string
import random
alfa = string.ascii_uppercase + string.ascii_lowercase
flag = "helsectf{rask_rust_rimer_relativt_riktig!}"
fake = "false_flags_are_not_cool_to_hunt_correctly"
print(f"let mut arr = {[x for x in fake]};".replace("'","\""))
scramble = [(i,pt) for i,pt in enumerate(flag)]
part0 = scramble[0:9]
part1 = scramble[9:-1]
part2 = scramble[-1:]
random.shuffle(part1)
scramble = [*part0, *part1, *part2]
for i,pt in scramble:
    print(f"arr[{i}] = \"{pt}\";")
print("""arr.reverse();
let result = arr.join("");""");