
module a
    implicit none
    !private
    !public flag
    
    character(len=44) :: ikke_sikkert_man_maa_reverse_filen = "hint hint hint hint hint hint hint hint hint"
    character(len=51) :: flag = 'gemredsf|k3oFe`r1E2n`e02j_qqoh`MNdR8d_j^Fpqts`n`80~'       
    character(len=44) :: kan_det_vaere_en_annen_l0sning__sp0rsmaalstegn = "hint hint hint hint hint hint hint hint hint"    

    contains

    subroutine d(ct)
        character(len=*), intent(in) :: ct
        integer :: i
        integer x
        do i = 1, len(ct), 1
            x = iachar(ct(i:i))
            print "(a,$)", achar(x - (modulo(i-1, 3) - 1))
        end do
        print "(a,$)", " is the real flag, not this: "
    end subroutine d

    character(51) function b(s) result(c)        
        integer, intent(in) :: s
        c = flag
        if (s>0) call d(c)
    end function
end module a

program main
  use a, only: b
  implicit none  
  print *, "flag=", b(0)
end program