Vi ser at __a_MOD_b blir kjørt fra MAIN__ med argumentet "0" (0x402040), og hvis argumentet er >0 så vil den kalle __a_MOD_d som gjør noe med strengen som spyttes ut. Vi kan derfor patche binarien slik at argumentet blir 1 og prøve å kjøre.

Det ligger også et par hint i filen som objektnavn:
  obj.__a_MOD_ikke_sikkert_man_maa_reverse_filen
  obj.__a_MOD_kan_det_vaere_en_annen_l0sning__sp0rsmaalstegn

# Ghidra: rev funksjon
```

void MAIN__(void)

{
  undefined local_258 [64];
  undefined4 local_218;
  undefined4 local_214;
  char *local_210;
  undefined4 local_208;
  
  local_210 = "main.f90";
  local_208 = 0x29;
  local_218 = 0x80;
  local_214 = 6;
  _gfortran_st_write(&local_218);
  _gfortran_transfer_character_write(&local_218,&DAT_0040203b,5);
  __a_MOD_b(local_258,0x33,&DAT_00402040);
  _gfortran_transfer_character_write(&local_218,local_258,0x33);
  _gfortran_st_write_done(&local_218);
  return;
}


void __a_MOD_b(void *param_1,undefined8 param_2,int *param_3)

{
  memmove(param_1,__a_MOD_flag,0x33);
  if (0 < *param_3) {
    __a_MOD_d(param_1,0x33);
  }
  return;
}



void __a_MOD_d(long param_1,int param_2)

{
  uint uVar1;
  undefined4 local_238;
  undefined4 local_234;
  char *local_230;
  undefined4 local_228;
  undefined *local_1e8;
  undefined8 local_1e0;
  char local_21;
  uint ch;
  int local_1c;
  
  for (local_1c = 1; local_1c <= param_2; local_1c = local_1c + 1) {
    ch = (uint)*(byte *)(param_1 + -1 + (long)local_1c);
    local_230 = "main.f90";
    local_228 = 0x16;
    local_1e8 = &DAT_00402019;
    local_1e0 = 5;
    local_238 = 0x1000;
    local_234 = 6;
    _gfortran_st_write(&local_238);
    uVar1 = local_1c - 1;
    local_21 = (char)ch -
               ((char)uVar1 +
                ((byte)((ulong)(uVar1 ^ (int)uVar1 >> 0x1f) * 0x55555556 >> 0x20) ^
                (byte)((int)uVar1 >> 0x1f)) * -3 + -1);
    _gfortran_transfer_character_write(&local_238,&local_21,1);
    _gfortran_st_write_done(&local_238);
  }
  local_230 = "main.f90";
  local_228 = 0x1c;
  local_1e8 = &DAT_00402019;
  local_1e0 = 5;
  local_238 = 0x1000;
  local_234 = 6;
  _gfortran_st_write(&local_238);
  _gfortran_transfer_character_write(&local_238,&DAT_0040201e,0x1d);
  _gfortran_st_write_done(&local_238);
  return;
}
```

Vi kan rydde opp litt i `__a_MOD_d` ved å sette typer og endre navn:
```

void __a_MOD_d(char *param_1,int size)

{
  undefined4 print_params;
  undefined4 local_234;
  char *local_230;
  undefined4 local_228;
  undefined *local_1e8;
  undefined8 local_1e0;
  char pt;
  uint ct;
  int i;
  uint x;
  
  for (i = 1; i <= size; i = i + 1) {
    ct = (uint)(byte)param_1[(long)i + -1];
    local_230 = "main.f90";
    local_228 = 0x16;
    local_1e8 = &DAT_00402019;
    local_1e0 = 5;
    print_params = 0x1000;
    local_234 = 6;
    _gfortran_st_write(&print_params);
    x = i - 1;
    pt = (char)ct -
         ((char)x + ((byte)((ulong)(x ^ (int)x >> 0x1f) * 0x55555556 >> 0x20) ^
                    (byte)((int)x >> 0x1f)) * -3 + -1);
    _gfortran_transfer_character_write(&print_params,&pt,1);
    _gfortran_st_write_done(&print_params);
  }
  local_230 = "main.f90";
  local_228 = 0x1c;
  local_1e8 = &DAT_00402019;
  local_1e0 = 5;
  print_params = 0x1000;
  local_234 = 6;
  _gfortran_st_write(&print_params);
  _gfortran_transfer_character_write(&print_params,&DAT_0040201e,0x1d);
  _gfortran_st_write_done(&print_params);
  return;
}
```

Vi kan rydde videre, kun i loopen, og dette kan vi nå dra rett inn i python.
```
  for (i = 1; i <= size; i = i + 1) {
    ct = (uint)(byte)param_1[(long)i + -1];
    x = i - 1;
    pt = ct - (x + (((x ^ x >> 0x1f) * 0x55555556 >> 0x20) ^ ( x >> 0x1f)) * -3 + -1) ;
    _gfortran_transfer_character_write(&print_params,&pt,1);
  }
```

```python
# Ghidra __a_MOD_flag @0x00404080 -> marker hele data feltet -> Copy special -> Python byte string
flag = b'\x67\x65\x6d\x72\x65\x64\x73\x66\x7c\x6b\x33\x6f\x46\x65\x60\x72\x31\x45\x32\x6e\x60\x65\x30\x32\x6a\x5f\x71\x71\x6f\x68\x60\x4d\x4e\x64\x52\x38\x64\x5f\x6a\x5e\x46\x70\x71\x74\x73\x60\x6e\x60\x38\x30\x7e\x00'
for i in range(1, len(flag)):
    ct = flag[i-1]
    x = i - 1;
    pt = ct - (x + (((x ^ x >> 0x1f) * 0x55555556 >> 0x20) ^ ( x >> 0x1f)) * -3 + -1)
    pt = chr(pt)
    print(pt, end="")
# helsectf{l3nGe_s1D3n_f01k_progaMMeR7e_i_Fortran_90}>
print()
```

# radare2: patch __a_MOD_b argument til 1

```
[0x00401090]> iz
[Strings]
nth paddr      vaddr      len size section type  string
―――――――――――――――――――――――――――――――――――――――――――――――――――――――
0   0x00002010 0x00402010 8   9    .rodata ascii main.f90
1   0x00002019 0x00402019 39  40   .rodata ascii (a,$) is the real flag, not this: flag=
0   0x00003080 0x00404080 51  52   .data   ascii gemredsf|k3oFe`r1E2n`e02j_qqoh`MNdR8d_j^Fpqts`n`80~
1   0x000030c0 0x004040c0 44  45   .data   ascii hint hint hint hint hint hint hint hint hint
2   0x00003100 0x00404100 44  44   .data   ascii hint hint hint hint hint hint hint hint hint

[0x00401176]> is~OBJ
39  0x0000037c 0x0040037c LOCAL  OBJ    32       __abi_tag
66  ---------- 0x0040412c LOCAL  OBJ    1        completed.0
67  0x00002dd8 0x00403dd8 LOCAL  OBJ    0        __do_global_dtors_aux_fini_array_entry
69  0x00002dd0 0x00403dd0 LOCAL  OBJ    0        __frame_dummy_init_array_entry
72  0x00002050 0x00402050 LOCAL  OBJ    28       options.5.0
74  0x0000220c 0x0040220c LOCAL  OBJ    0        __FRAME_END__
77  0x00003000 0x00404000 LOCAL  OBJ    0        _GLOBAL_OFFSET_TABLE_
80  0x00002de0 0x00403de0 LOCAL  OBJ    0        _DYNAMIC
87  0x00002000 0x00402000 GLOBAL OBJ    4        _IO_stdin_used
91  0x00003080 0x00404080 GLOBAL OBJ    51       __a_MOD_flag
92  ---------- 0x00404130 GLOBAL OBJ    0        __TMC_END__
93  0x00002008 0x00402008 GLOBAL OBJ    0        __dso_handle
94  0x00003100 0x00404100 GLOBAL OBJ    44       __a_MOD_kan_det_vaere_en_annen_l0sning__sp0rsmaalstegn
96  0x000030c0 0x004040c0 GLOBAL OBJ    44       __a_MOD_ikke_sikkert_man_maa_reverse_filen


[0x004013f6]> s sym.MAIN__
[0x00401351]> pdf
            ; CALL XREF from main @ 0x401425
┌ 165: sym.MAIN__ ();
│           ; var int64_t var_250h @ rbp-0x250
│           ; var int64_t var_210h @ rbp-0x210
│           ; var int64_t var_20ch @ rbp-0x20c
│           ; var char *var_208h @ rbp-0x208
│           ; var int64_t var_200h @ rbp-0x200
│           0x00401351      55             push  (rbp)
│           0x00401352      4889e5         rbp = rsp
│           0x00401355      4881ec500200.  rsp -= 0x250
│           0x0040135c      48c785f8fdff.  qword [var_208h] = str.main.f90 ; 0x402010 ; "main.f90"
│           0x00401367      c78500feffff.  dword [var_200h] = 0x22     ; '\"' ; 34
│           0x00401371      c785f0fdffff.  dword [var_210h] = 0x80     ; 128
│           0x0040137b      c785f4fdffff.  dword [var_20ch] = 6
│           0x00401385      488d85f0fdff.  rax = var_210h
│           0x0040138c      4889c7         rdi = rax
│           0x0040138f      e8ecfcffff     sym.imp._gfortran_st_write  ()
│           0x00401394      488d85f0fdff.  rax = var_210h
│           0x0040139b      ba05000000     edx = 5
│           0x004013a0      be3b204000     esi = 0x40203b              ; '; @' ; "flag="
│           0x004013a5      4889c7         rdi = rax
│           0x004013a8      e883fcffff     sym.imp._gfortran_transfer_character_write  ()
│           0x004013ad      488d85b0fdff.  rax = var_250h
│           0x004013b4      ba40204000     edx = 0x402040              ; '@ @' ; int64_t arg3
│           0x004013b9      be33000000     esi = 0x33                  ; '3' ; 51 ; int64_t arg2
│           0x004013be      4889c7         rdi = rax                   ; void *arg1
│           0x004013c1      e8b0fdffff     sym.__a_MOD_b  ()
│           0x004013c6      488d8db0fdff.  rcx = var_250h
│           0x004013cd      488d85f0fdff.  rax = var_210h
│           0x004013d4      ba33000000     edx = 0x33                  ; '3' ; 51
│           0x004013d9      4889ce         rsi = rcx
│           0x004013dc      4889c7         rdi = rax
│           0x004013df      e84cfcffff     sym.imp._gfortran_transfer_character_write  ()
│           0x004013e4      488d85f0fdff.  rax = var_210h
│           0x004013eb      4889c7         rdi = rax
│           0x004013ee      e85dfcffff     sym.imp._gfortran_st_write_done  ()
│           0x004013f3      90             no
│           0x004013f4      c9             leav
└           0x004013f5      c3             re

[0x00401351]> px16 @ 0x402040
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x00402040  0000 0000 0000 0000 0000 0000 0000 0000  ................
[0x00401351]> w1+ @ 0x402040   ; increment 1 byte at <..>
[0x00401351]> px16 @ 0x402040
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x00402040  0100 0000 0000 0000 0000 0000 0000 0000  ................
[0x00401351]> dc
 flag=helsectf{l3nGe_s1D3n_f01k_progaMMeR7e_i_Fortran_90} is the real flag, not this: gemredsf|k3oFe`r1E2n`e02j_qqoh`MNdR8d_j^Fpqts`n`80~
(1020771) Process exited with status=0x0
```