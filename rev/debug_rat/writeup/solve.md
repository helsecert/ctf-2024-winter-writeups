Det var 2 løsninger:

# alternativ1 (solve.py) reverse EXEC interfacet.
EXEC er base64(xor(cmd, xorkey)) og xorkey settes med DEBUG <inpt> hvor xorkey=inpt^HELSECTF_IS_SO_MUCH_FUN\x00
Nå får man remote code exection mot remote.

# alternativ2 reverse CAT og SIGNAL
Det lå inne en CAT kommando. Den hadde et fjerde element i sin array som ble aktivert hvis man slang på argumentet "1337"

det var satt en alarm(2) som hvis "SIGNAL" var kjørt så ville den printe flagget.

Noen deltakere poengterte at man kunne skrive inn hva som helst som bare var 6 tegn. Dette var en feil.