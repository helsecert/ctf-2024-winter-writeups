rm debug_rat 2>/dev/null
rm debug_rat.zip 2>/dev/null
docker build -t debug_rat_build .
docker rm -f cp 2>/dev/null
docker cp $(docker create --name cp debug_rat_build:latest):/home/ctf/debug_rat ./debug_rat && docker rm cp
zip --password infected debug_rat.zip debug_rat Dockerfile Makefile

rm -rf ./tmp 2>/dev/null
sh -c "mkdir tmp && cd tmp && cp ../debug_rat.zip . && unzip -P infected debug_rat.zip && md5sum debug_rat Dockerfile Makefile debug_rat.zip"
echo ""
md5sum debug_rat Dockerfile Makefile debug_rat.zip

cp debug_rat.zip ~/Desktop/