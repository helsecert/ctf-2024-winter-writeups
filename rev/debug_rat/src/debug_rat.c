#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
//#include <resolv.h>
#include "base64.h"

/*
void debug(char * buf, long s) {
    int i;
    for (i = 0; i < s; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", buf[i]);
    }
    printf("\n");
}
*/

// FLAG part 1 is in the cat.
unsigned char MYCAT[218] ;

// xor(b"flag part2: _Netbus_aNd_Back", b"\x03\x05")
char FLAGPART2[] = {0x65,0x69,0x62,0x62,0x23,0x75,0x62,0x77,0x77,0x37,0x39,0x25,0x5c,0x4b,0x66,0x71,0x61,0x70,0x70,0x5a,0x62,0x4b,0x67,0x5a,0x41,0x64,0x60,0x6e, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
int FLAGPART2LEN = 28 ;
char FLAGPART2KEY[2] = {0x03,0x05} ;

char FLAGPART3[] = "You need moar skillz to get the whole flag. A start is this flag part: Orifice???}" ;

// XORKEY for print
char PRINT_XOR_KEY[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,};
char * pprintptr = &(PRINT_XOR_KEY) ;

// SECRET to xor EXEC commands
unsigned char SECRET[32];

// EXEC required MAGIC
char MAGIC[] = {0x48, 0x45, 0x4c, 0x53, 0x45, 0x43, 0x54, 0x46, 0x5f, 0x49, 0x53, 0x5f, 0x53, 0x4f, 0x5f, 0x4d, 0x55, 0x43, 0x48, 0x5f, 0x46, 0x55, 0x4e, 0x00};

// DEBUG command to set SECRET xored with MAGIC
char DEBUG[] = "AAAAA" ;
uint8_t * PTRDEBUG ;

// Taunts
#define cool "helsectf is a cool CTF, right?"
#define noflaghere "you will not find the whole flag here!"

int signalticks = 0 ;
int alarmflag = 0 ;
int null = 0 ;
int H = 0x41 ;
int b = 65 ;

void prnt(char * buf, long s) {
    int i;
    for (i = 0; i < s; i++) printf("%c", buf[i]);
    printf("\n");
}

static __attribute__ ((noinline)) void PRINT_XOR_KEY_bytepart1() {
    // we set the missing byte first time for PRINT_XOR_KEY[1] = 0x02;
    uint8_t * a = pprintptr ;
    int N = 10 ;
    for(;N>2;N--);
    int i = 0x42;
    int x = 0x41;
    for(;x>0;x--) i-- ;
    *(a+1) = N;
}

void check_alarm(int sig) {
    //printf("signal\n");
    if(sig == SIGALRM) {
        signalticks += 1 ;
    }

    if(PRINT_XOR_KEY[1]^null^H^b == 0) PRINT_XOR_KEY_bytepart1();

    if(signalticks >= 15) {
        printf("INTRUDER! Your hacking was notified!\n");
        _exit(0);
    }

    if(alarmflag == 1 ) {
        char * buf[FLAGPART2LEN] ;
        copy(&FLAGPART2, buf, FLAGPART2LEN) ;
        prnt(buf, FLAGPART2LEN);  //  flag part2: _Netbus_aNd_Back
        alarmflag = 0;
    }

    alarm(2);
}

__attribute__ ((noinline)) void setup_debug_string() {        
    if(DEBUG[0] == 0x41) {
        strncpy(DEBUG, "DRCFG", 5); // xor(b"DEBUG"[::-1], [0x3,0x7,0x1,0x3])
    }
}

void __attribute__ ((constructor)) first_alarm() {
    check_alarm(0xff);
    setup_debug_string() ;
}

void __attribute__ ((constructor)) __init__() {
    const char * Z = cool;
    if(FLAGPART2[0] != 0x5f) {        
        int i = FLAGPART2LEN;    
        int z = 1 ;            
        char * ptr = &FLAGPART2;        
        for(i=0;i<FLAGPART2LEN;i += z) {
            ptr[i] = ptr[i] ^ FLAGPART2KEY[i%2]; // decrypt flag part2
        }
        FLAGPART2KEY[0] = 0x00;
        FLAGPART2KEY[1] = 0x00; // kill the key
    }    
    PRINT_XOR_KEY[7] = 0x03; 
}

void please_dont_buffer() {
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    if(PRINT_XOR_KEY[5] + 0xff == 0xff) {
           PRINT_XOR_KEY[5] = 0x04;
    }
    signal(SIGALRM,check_alarm);
    alarm(1);
}

__attribute__ ((noinline)) void decrypt_debug() {
    char KEY[] =  {0x3,0x7,0x1,0x3};
    krypter(PTRDEBUG, 5, KEY,4); 
    return ;
}

__attribute__ ((noinline)) void krypter(char * buf, long bsize, char * key, long ksize) {
    int i;	    
	for(i=0;i<bsize;i++) {
        buf[i] = buf[i] ^ key[i%ksize];
	}
}

static __attribute__ ((noinline)) int krypter_et_tegn(int b, int k) {    
    return (b ^ k) | b ;
}

__attribute__ ((noinline)) void dekryptprint(char * buf, long l) {
    krypter(buf, l, PRINT_XOR_KEY, 8);    
}

void __attribute__ ((constructor)) load_my_cat() {
    int fd = open("cat_ascii.txt", O_RDONLY);
    read(fd, MYCAT, 218);
    close(fd);
}

__attribute__ ((noinline)) void kryptoprint(char * buf, long s) {            
    krypter(buf, s, PRINT_XOR_KEY, 8);
    printf("%s\n", buf) ;
}

__attribute__ ((noinline)) void turnaround(char * s, char * d, long l) {    
    int i ;
    for(i=l-1;i>=0;i--) {
        d[l-i-1] = s[i] ;
    }
}

static __attribute__ ((noinline)) void hint() {    
    int i ;
    unsigned char arr[77] ;
    int z = 10 ;
    const char * X = noflaghere ;
    printf(X);
    printf("\n");     

    arr[28] = 111; arr[1] = 114; arr[13] = 111; arr[31] = 109; arr[27] = 121; arr[44] = 116; arr[7] = 100; arr[12] = 116; arr[26] = 32; arr[32] = 117; arr[21] = 32; arr[30] = 32; arr[45] = 111; arr[37] = 101; arr[16] = 97; arr[0] = 116; arr[29] = 117; arr[19] = 32; arr[10] = 44; arr[22] = 102; arr[47] = 114; arr[38] = 32; arr[11] = 32; arr[18] = 110; arr[49] = 110; arr[39] = 97; arr[51] = 32; arr[41] = 108; arr[40] = 98; arr[14] = 32; arr[5] = 97; arr[8] = 101; arr[36] = 98; arr[15] = 101; arr[33] = 115; arr[17] = 114; arr[48] = 117; arr[9] = 114; arr[34] = 116; arr[20] = 97; arr[6] = 114; arr[46] = 32; arr[4] = 104; arr[23] = 108; arr[2] = 121; arr[43] = 32; arr[3] = 32; arr[25] = 103; arr[24] = 97; arr[42] = 101; arr[35] = 32; arr[50] = 58;
    for (i = 0; i < z; i++) {
        printf("%c", arr[i]);    
    }    
    printf("\n");     
    if ((i^z) < 0x1337) { return ;}

    z = 51;
    for (i = (6^10); i < z; i++) printf("%c", arr[i]);    
    printf("\n"); 

    if(krypter_et_tegn(0x41, 0x42) == 0x43 ) return ;
    arr[52] = krypter_et_tegn(69, 69);
    arr[53] = krypter_et_tegn(88, 88);
    arr[54] = krypter_et_tegn(69, 69);
    arr[55] = krypter_et_tegn(67, 67);
    arr[56] = krypter_et_tegn(32, 32);
    arr[57] = krypter_et_tegn(39, 39);
    arr[58] = krypter_et_tegn(47, 47);
    arr[59] = krypter_et_tegn(98, 98);
    arr[60] = krypter_et_tegn(105, 105);
    arr[61] = krypter_et_tegn(110, 110);
    arr[62] = krypter_et_tegn(47, 47);
    arr[63] = krypter_et_tegn(99, 99);
    arr[64] = krypter_et_tegn(97, 97);
    arr[65] = krypter_et_tegn(116, 116);
    arr[66] = krypter_et_tegn(32, 32);
    arr[67] = krypter_et_tegn(47, 47);
    arr[68] = krypter_et_tegn(102, 102);
    arr[69] = krypter_et_tegn(108, 108);
    arr[70] = krypter_et_tegn(97, 97);
    arr[71] = krypter_et_tegn(103, 103);
    arr[72] = krypter_et_tegn(46, 46);
    arr[73] = krypter_et_tegn(116, 116);
    arr[74] = krypter_et_tegn(120, 120);
    arr[75] = krypter_et_tegn(116, 116);
    arr[76] = krypter_et_tegn(39, 39);    
    for (z = 52; z < 77; z++) printf("%c", arr[z]);    
    printf("\n");
    
    char * lastflagpartptr[82] ;
    copy(&FLAGPART3, lastflagpartptr, 82) ;
    prnt(lastflagpartptr, 82);  //  flag part3: Orifice???}
    int *ptr = &arr;
    *ptr = 543451492;
    *(ptr+1) = 1952540788;
    *(ptr+2) = 1818585120;
    *(ptr+3) = 1870209136;
    arr[16] = 117;
    arr[17] = 63;
    arr[18] = 63;
    arr[19] = 0x0a ;
    arr[20] = 0x00 ;
    for (z = 0; z < 21; z++) printf("%c", arr[z]);        
}

__attribute__ ((noinline)) void copy(char * s, char * d, long l) {    
    int i ;
    for(i=0;i<l;i++) d[i] = s[i] ;
}

int main(int argc, char *argv[]) {    
    char welcome[] = "Smf`d}r#RCT!v4032201140342013)dfbwg\x0b";
    char help[] = "HGLQ:$LP AAU AXFC\x08";
    char input[128];
    int i ;
    PRINT_XOR_KEY[3] = 0x01;
    PTRDEBUG = &DEBUG ;
    please_dont_buffer();
    kryptoprint(welcome,36);    
    dekryptprint(help, 18) ;
    printf("%s\n",help) ;    
    decrypt_debug() ;

    for(;;) {                
        printf("> ") ;
        fgets(input, 128, stdin);        

        int l ;
        for(l=0;l<128;l++) if(!input[l]) break ;

        l -= 1; // newline
        switch (l) {
            case 4:                
                if (strncmp(input, "HELP", 4) == 0x0) {
                    printf("%s\n", help) ;
                    continue;
                }                
                else if (input[2] == 78 && input[0] == 72 && input[3] == 84 && input[1] == 73) {
                    // "HINT"
                    hint();                    
                    continue;
                }
                break;        
            case 2:
                if (strncmp(input, "LS", 2) == 0x0) {
                    system("/bin/ls");                    
                    continue;
                }
                break ;
        }        

        if (l >= 3) {                      
            if ((input[0]^75) == 8 && (input[1]^69) == 4 && (input[2]^89) == 13) {                
                if (MYCAT[0] == 0x00) {
                    load_my_cat();                    
                }
                const char *cats[3];
                cats[0] = "Meow! I WISH I COULD CAT A FILE";
                cats[1] = "ONE DAY I WILL COMPLETE MY CAT COMMAND";
                cats[2] = "YOUR GOAL IS TO: EXEC cat /flag.txt" ;
                cats[3] = MYCAT; // hidden ascii with flag part1: helsectf{r3meMber_
                int n = rand()%3;
                // can patch n = 3 to show cat
                if (l == 8 && input[3] == 0x20 && input[4] == 0x31 && input[5] == 0x33 && input[6] == 0x33 && input[7] == 0x37) {
                    n = 3; // flag part1
                }                
                printf("%s\n", cats[n]);
                continue;
            }

            // DEBUG to set SECRET 
            char Z[5];            
            turnaround(input, Z, 5);
            if (l >= 5 && strncmp(Z, DEBUG, 5) == 0) {
                // Extra hint: xor(b"TODO: remove DEBUG interface to prevent SECRET key overwrite\n", b"\x00\x02\x00\x01\x00\x04\x00\x03")
                char hint[] = "TMDN:$rfmmvd @EAUE hnpeqfccd po#ppewejt#SGCSEP he{ nvartrktd\n";
                kryptoprint(hint, 65);
                if(l == 38 && (l-6) == 32) {
                    uint8_t *ptr = &input ;
                    ptr += 6 ;
                    memcpy(SECRET, ptr, 32);       
                    krypter(SECRET, 32, MAGIC, 24);
                    //debug(SECRET, 32);
                    printf("OK SECRET overwrite\n") ;
                    continue; 
                }
                printf("ERROR length mismatch\n");
                continue;
            }

            // SET alarmflag to get flag2 with "SIGNAL"
            if (l==6 && input[0]^0x7 == 0x54 && input[1]^0xff == 0xb6 && input[2]^0x54 == 0x13 && input[3]^0x8a == 0xc4 && input[4]^0x3 == 0x42 && input[5]^0xa2 == 0xee) {                
                alarmflag = 1 ;
                continue;
            }
        
            if (l >= 4 && strncmp(input, "EXEC", 4) == 0) {
                input[l] = 0x0;
                uint8_t *ptr = &input ;                
                l -= 5 ;
                ptr += 5 ;

                // Base64 decode
                size_t out_len = b64_decoded_size(ptr)  ;
                if(out_len > 128) {
                    printf("EXEC too much data\n");
                    _exit(0) ;
                }
                char * out = malloc(out_len) ;
                int err = b64_decode(ptr, (unsigned char *)out, out_len);
                if(err == 0) {
                    printf("EXEC malformed input\n");
                    continue ;
                }

                // Decrypt and check magic
                out[out_len] = 0x0;
                krypter(out, out_len, SECRET, 32);                
                if(memcmp(MAGIC, out, 24) != 0) {
                    printf("EXEC MAGIC mismatch\n");
                    continue ;
                }
                out += 24 ; // skip MAGIC
                out_len -= 24 ;
                char * buf[out_len];
                memcpy(buf, out, out_len) ;

                // Run command!
                char res[1024] = {0x0};
                FILE *fp = popen(out, "r");
                if (fp == NULL) {
                    printf("EXEC RUN failed\n") ;
                    _exit(0) ;
                }
                printf("EXEC result:\n");
                while (fgets(res, sizeof(res), fp) !=NULL) {                    
                    int L = strlen(res);
                    krypter(res, L, SECRET, 32);                    
                    uint8_t * ptr = &res ;
                    base64_print(ptr);
                }              
                pclose(fp) ;
                printf("> EXEC SUCCESS\n");
                continue;                
            }
        }
        printf("error\n");
    }

}

void __attribute__  ((constructor)) __rnd_secret() {
    int fd = open("/dev/urandom", O_RDONLY);
    read(fd, SECRET, 32);
    close(fd);
}
