#ifndef _BASE64_HEADER_H
#define _BASE64_HEADER_H
#include <stdio.h>
size_t b64_encoded_size(size_t inlen);
#endif 