sh -c "cd src/; bash build.sh;"

docker-compose build --force-rm
docker-compose up --remove-orphans -d

ls -lah src/
nc -zv localhost 8003
